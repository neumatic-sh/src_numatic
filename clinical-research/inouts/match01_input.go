package inouts

import (
	"strings"
	"time"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
	"bitbucket.org/neumatic-sh/clinical-research/global"
)

type Match01Input struct {
	validutil.ValidatorErr `,inline`
	Gender                 int `json:"gender" form:"gender"`
	Birthday               time.Time
	BirthdayStr            string   `json:"birthdayStr" form:"birthdayStr"`
	IDNo                   string   `json:"IDNo" form:"IDNo"`
	Ethnic                 string   `json:"ethnic" form:"ethnic"`
	EthnicRadio            int      `json:"ethnicRadio" form:"ethnicRadio"`
	Address                string   `json:"address" form:"address"`
	Relative               string   `json:"relative" form:"relative"`
	Relationship           string   `json:"relationship" form:"relationship"`
	Phone                  []string `json:"phone" form:"phone"`
	HospitalNo             string   `json:"hospitalNo" form:"hospitalNo"`
	Status                 global.MatchStatus
}

func (this *Match01Input) ValidateSubmit() bool {

	var (
		err    error
		phones = []string{}
	)

	this.IDNo = strings.TrimSpace(this.IDNo)
	this.Address = strings.TrimSpace(this.Address)
	this.Relative = strings.TrimSpace(this.Relative)
	this.Relationship = strings.TrimSpace(this.Relationship)
	this.HospitalNo = strings.TrimSpace(this.HospitalNo)
	this.Ethnic = strings.TrimSpace(this.Ethnic)

	for _, phone := range this.Phone {
		if phone == "" {
			continue
		}
		phones = append(phones, strings.TrimSpace(phone))
	}
	this.Phone = phones

	// err = goutils.ValidateID(this.IDNo)
	// if err != nil {
	// 	this.AddError("IDNo", err)
	// }

	this.ValidField(this.Address, "address", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	this.ValidField(this.Relative, "relative", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	this.ValidField(this.Relationship, "relationship", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	this.ValidField(this.Phone, "phone", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	this.ValidField(this.HospitalNo, "hospitalNo", "nonzero", errorutil.ErrShouldNotBeBlank.Error())

	if this.Gender != 1 && this.Gender != 2 {
		this.AddError("gender", errorutil.ErrShouldNotBeBlank)
	}

	if this.EthnicRadio == 1 {
		this.Ethnic = "汉族"
	} else if this.EthnicRadio == 2 {
		this.ValidField(this.Ethnic, "ethnic", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	} else {
		this.AddError("ethnicRadio", errorutil.ErrShouldNotBeBlank)
	}

	this.ValidField(this.BirthdayStr, "birthdayStr", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	if this.BirthdayStr != "" {
		this.Birthday, err = time.Parse(global.TIME_LAYOUT_DATE, this.BirthdayStr)
		if err != nil {
			this.AddError("birthdayStr", errorutil.ErrInvalidTimeFormat)
		}
	}

	return this.HasError()
}
