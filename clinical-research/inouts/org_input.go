package inouts

import (
	"strings"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
)

type OrgInput struct {
	validutil.ValidatorErr `,inline`
	Name                   string
	Code                   string
}

func (this *OrgInput) ValidateCreate() bool {
	this.Name = strings.TrimSpace(this.Name)
	this.Code = strings.TrimSpace(this.Code)

	this.ValidField(this.Name, "Name", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	this.ValidField(this.Code, "Code", "nonzero", errorutil.ErrShouldNotBeBlank.Error())

	return this.HasError()
}

func (this *OrgInput) ValidateUpdate() bool {
	return this.ValidateCreate()
}
