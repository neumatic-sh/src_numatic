package inouts

import (
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"gopkg.in/mgo.v2/bson"
)

type Match16Input struct {
	validutil.ValidatorErr `,inline`
	Id                     string
	ICHDisabled            bool
	ICHScore               int
	GCS                    int
	EyeResp                int
	LangResp               int
	BodyResp               int
	HematomaVol            int
	HematomaIn             int
	HematomaFrom           int
	AgeScore               int
	GraebDisabled          bool
	GraebScore             int
	LeftBrain              int
	RightBrain             int
	ThirdBrain             int
	FourthBrain            int
	HuntHessDisabled       bool
	HuntHessLevel          int
	FisherDisabled         bool
	FisherLevel            int
	Status                 global.MatchStatus
}

func (this *Match16Input) ValidateSubmit() bool {

	var (
		twoOptionMap  = BuildOptionMap(1, 2)
		fourOptionMap = BuildOptionMap(1, 4)
		fiveOptionMap = BuildOptionMap(1, 5)
		sixOptionMap  = BuildOptionMap(1, 6)
	)

	if !this.ICHDisabled {

		if !BuildOptionMap(0, 3).contain(this.GCS) {
			this.AddError("GCS", errorutil.ErrNoSelection)

		}

		if !fourOptionMap.contain(this.EyeResp) {
			this.AddError("EyeResp", errorutil.ErrNoSelection)
		}

		if !fiveOptionMap.contain(this.LangResp) {
			this.AddError("LangResp", errorutil.ErrNoSelection)
		}

		if !sixOptionMap.contain(this.BodyResp) {
			this.AddError("BodyResp", errorutil.ErrNoSelection)
		}

		if !BuildOptionMap(0, 2).contain(this.HematomaVol) {
			this.AddError("HematomaVol", errorutil.ErrNoSelection)
		}

		if !BuildOptionMap(0, 2).contain(this.HematomaIn) {
			this.AddError("HematomaIn", errorutil.ErrNoSelection)
		}

		if !BuildOptionMap(0, 2).contain(this.HematomaFrom) {
			this.AddError("HematomaFrom", errorutil.ErrNoSelection)
		}

		if !BuildOptionMap(0, 2).contain(this.AgeScore) {
			this.AddError("AgeScore", errorutil.ErrNoSelection)
		}
	}

	if !this.GraebDisabled {
		if !fourOptionMap.contain(this.LeftBrain) {
			this.AddError("LeftBrain", errorutil.ErrNoSelection)
		}

		if !fourOptionMap.contain(this.RightBrain) {
			this.AddError("RightBrain", errorutil.ErrNoSelection)
		}

		if !twoOptionMap.contain(this.ThirdBrain) {
			this.AddError("ThirdBrain", errorutil.ErrNoSelection)
		}

		if !twoOptionMap.contain(this.FourthBrain) {
			this.AddError("FourthBrain", errorutil.ErrNoSelection)
		}
	}

	if !this.HuntHessDisabled {
		if !fiveOptionMap.contain(this.HuntHessLevel) {
			this.AddError("HuntHessLevel", errorutil.ErrNoSelection)
		}

	}

	if !this.FisherDisabled {
		if !fourOptionMap.contain(this.FisherLevel) {
			this.AddError("FisherLevel", errorutil.ErrNoSelection)
		}
	}

	if !bson.IsObjectIdHex(this.Id) {
		this.AddError("Id", errorutil.ErrInvalidId)
	}

	return this.HasError()
}
