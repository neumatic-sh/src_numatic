package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
)

func ShowMatchOld(c echo.Context) (err error) {

	var (
		theCase *models.Case
		survey  *models.Survey
		org     *models.Organization
		caseId  = c.Param("id")
		match   = c.Param("match")
		matchId = c.Param("matchId")
	)

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	err = mgowrap.FindById(theCase.OrgID, &org)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	survey, err = models.FindSurveyByCaseId(theCase.Id)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// TODO: temp hack this for the frontend
	if survey.Match01.Ethnic == "汉族" {
		survey.Match01.Ethnic = ""
	}

	result := map[string]interface{}{
		"Case":   theCase,
		"Survey": survey,
		"Org":    org,
		"Match":  nil,
	}

	// the special case
	if match == "match16" {
		var theMatch *models.Match16
		for _, m := range survey.Match16s {
			if m.Id.Hex() == matchId {
				theMatch = m
			}
		}

		if theMatch == nil {
			return errorutil.ErrNotFound
		}

		result["Match"] = theMatch
	}

	return c.Render(http.StatusOK, fmt.Sprintf("survey/show_%s", match), result)
}

func SubmitMatch(c echo.Context) (err error) {

	var (
		theCase     *models.Case
		survey      *models.Survey
		me          = GetUser(c)
		caseId      = c.Param("id")
		match       = c.Param("match")
		matchId     = c.Param("matchId")
		redirectUrl = fmt.Sprintf("/cases/%s/%s", caseId, match)
	)

	if matchId != "" {
		redirectUrl = fmt.Sprintf("%s/%s", redirectUrl, matchId)
	}

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	survey, err = models.FindSurveyByCaseId(theCase.Id)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	switch match {
	case "match01":
		var (
			match01Input = &inouts.Match01Input{
				Status: global.MatchFinish,
			}
		)

		if err = c.Bind(match01Input); err != nil {
			return
		}

		// Validation
		if match01Input.ValidateSubmit() {
			webutil.FlashErrors(c, match01Input.ErrorMap)
			webutil.FlashParams(c)
			return c.Redirect(http.StatusFound, redirectUrl)
		}

		err = models.UpdateMatch01(theCase.Id, me.Id, match01Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	case "match02":
		var (
			match02Input = &inouts.Match02Input{
				Status: global.MatchFinish,
			}
		)

		if err = c.Bind(match02Input); err != nil {
			return
		}

		// Validation
		if match02Input.ValidateSubmit() {
			webutil.FlashErrors(c, match02Input.ErrorMap)
			webutil.FlashParams(c)
			return c.Redirect(http.StatusFound, redirectUrl)
		}

		err = models.UpdateMatch02(theCase.Id, me.Id, match02Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	case "match03":
		var (
			match03Input = &inouts.Match03Input{
				Status: global.MatchFinish,
			}
		)

		if err = c.Bind(match03Input); err != nil {
			return
		}

		// Validation
		if match03Input.ValidateSubmit() {
			webutil.FlashErrors(c, match03Input.ErrorMap)
			webutil.FlashParams(c)
			return c.Redirect(http.StatusFound, redirectUrl)
		}

		err = models.UpdateMatch03(theCase.Id, me.Id, match03Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	case "match16":
		var (
			match16Input = &inouts.Match16Input{
				Status: global.MatchFinish,
			}
		)

		if err = c.Bind(match16Input); err != nil {
			return
		}

		// Validation
		if match16Input.ValidateSubmit() {
			webutil.FlashErrors(c, match16Input.ErrorMap)
			webutil.FlashParams(c)
			return c.Redirect(http.StatusFound, redirectUrl)
		}

		err = models.UpdateMatch16(theCase.Id, me.Id, match16Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}

	}

	webutil.FlashInfo(c, "info", "操作成功！")
	return c.Redirect(http.StatusFound, redirectUrl)
}

func NewMatch(c echo.Context) (err error) {
	var (
		theCase    *models.Case
		survey     *models.Survey
		caseId     = c.Param("id")
		matchType  = c.Param("match")
		newMatch16 *models.Match16
	)

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	survey, err = models.FindSurveyByCaseId(theCase.Id)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	switch matchType {
	case "match16":
		newMatch16, err = models.NewSurveyMatch16(survey.Id)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	}

	return c.Redirect(http.StatusFound, fmt.Sprintf("/cases/%s/%s/%s", caseId, matchType, newMatch16.Id.Hex()))
}

func DraftMatch(c echo.Context) (err error) {

	var (
		theCase     *models.Case
		survey      *models.Survey
		me          = GetUser(c)
		caseId      = c.Param("id")
		match       = c.Param("match")
		matchId     = c.Param("matchId")
		redirectUrl = fmt.Sprintf("/cases/%s/%s", caseId, match)
	)

	if matchId != "" {
		redirectUrl = fmt.Sprintf("%s/%s", redirectUrl, matchId)
	}

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	survey, err = models.FindSurveyByCaseId(theCase.Id)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	switch match {
	case "match01":
		var (
			match01Input = &inouts.Match01Input{
				Status: global.MatchDraft,
			}
		)

		if err = c.Bind(match01Input); err != nil {
			return
		}

		err = models.UpdateMatch01(theCase.Id, me.Id, match01Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	case "match02":
		var (
			match02Input = &inouts.Match02Input{
				Status: global.MatchDraft,
			}
		)

		if err = c.Bind(match02Input); err != nil {
			return
		}

		err = models.UpdateMatch02(theCase.Id, me.Id, match02Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	case "match03":
		var (
			match03Input = &inouts.Match03Input{
				Status: global.MatchDraft,
			}
		)

		if err = c.Bind(match03Input); err != nil {
			return
		}

		err = models.UpdateMatch03(theCase.Id, me.Id, match03Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
	case "match16":
		var (
			match16Input = &inouts.Match16Input{
				Status: global.MatchDraft,
			}
		)

		if err = c.Bind(match16Input); err != nil {
			return
		}

		err = models.UpdateMatch16(theCase.Id, me.Id, match16Input, survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}

	}

	webutil.FlashInfo(c, "info", "暂存成功！")
	return c.Redirect(http.StatusFound, redirectUrl)
}
