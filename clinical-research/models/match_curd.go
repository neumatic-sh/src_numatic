package models

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"github.com/kobeld/mgowrap"
	"gopkg.in/mgo.v2/bson"
)

func FindAllMatches(query bson.M, sortFields ...string) (matches []*Match, err error) {

	if query == nil {
		query = bson.M{}
	}

	query["deletedat"] = bson.M{"$exists": false}

	err = mgowrap.FindAll(query, &matches, sortFields...)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func FindMatch(query bson.M) (match *Match, err error) {
	if query == nil {
		query = bson.M{}
	}
	query["deletedat"] = bson.M{"$exists": false}

	err = mgowrap.Find(query, &match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func FindAllEnabledMatches(query bson.M) (matches []*Match, err error) {

	query = bson.M{
		"enabled": true,
	}

	// sort by name
	return FindAllMatches(query, "name")
}

func DeleteMatchByIdHex(id string) (err error) {
	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&Match{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}
