package webutil

import (
	"errors"
	"fmt"
	"log"
	"net"
	"net/rpc"
	"reflect"

	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

type serverEngine struct {
	appConf Config
	rpc.Server
}

func NewServerEngine(conf Config, env string, confPath string) *serverEngine {

	val := reflect.ValueOf(conf)
	if val.Kind() != reflect.Ptr {
		panic(errors.New("The conf must be a pointer"))
	}

	loadConfigAndSetupEnv(conf, env, confPath)

	return &serverEngine{
		appConf: conf,
	}

}

func (this *serverEngine) RunRPC() {
	var (
		rpcPort = this.appConf.GetRpcPort()
	)

	if rpcPort == "" {
		panic("Error: No RPC port!")
	}

	log.Printf("Listening and serving RPC on port %s\n", rpcPort)
	listen, err := net.Listen("tcp", rpcPort)
	if err != nil {
		panic(err)
	}

	for {
		conn, err := listen.Accept()
		if debugutil.HasErrorAndPrintStack(err) {
			continue
		}

		go this.ServeConn(conn)
	}
}

func (this *serverEngine) RunEcho(engine *echo.Echo) {

	if engine == nil {
		engine = echo.New()
	}

	var (
		httpPort = this.appConf.GetHttpPort()
	)

	if httpPort == "" {
		panic("Error: No HTTP port!")
	}

	log.Printf("Listening and serving HTTP on port %s\n", httpPort)
	engine.Start(httpPort)
}

func LoadConfigAndSetupEnv(conf Config, env string, configPath string) {
	loadConfigAndSetupEnv(conf, env, configPath)
}

// ===== Private =====
func loadConfigAndSetupEnv(conf Config, env string, configPath string) {

	viper.SetConfigFile(configPath)

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	err = viper.UnmarshalKey(fmt.Sprintf("%s.%s", conf.GetName(), env), conf)
	if err != nil {
		panic(err)
	}

	conf.SetConfigFile(configPath)

	conf.AfterInit()

	log.Printf("\nThe configuration is: \n%+v\n\n", conf)
}

// wrap HTTPS in
func (this *serverEngine) RunEchoHTTPS(engine *echo.Echo) {

	if engine == nil {
		engine = echo.New()
	}

	var (
		httpsPort = this.appConf.GetHttpsPort()
		certFile  = this.appConf.GetCertFile()
		keyFile   = this.appConf.GetKeyFile()
	)

	if httpsPort == "" {
		panic("Error: No HTTPS port!")
	}
	if certFile == "" {
		panic("Error: No cert file!")
	}
	if keyFile == "" {
		panic("Error: No key file!")
	}

	log.Printf("Listening and serving HTTPS on port %s\n", httpsPort)
	engine.StartTLS(httpsPort, certFile, keyFile)
}
