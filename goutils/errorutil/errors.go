package errorutil

import (
	"fmt"

	"gopkg.in/mgo.v2"

	"errors"
)

var (
	ErrInvalidId           error = errors.New("无效的 ID")
	ErrInvalidParam        error = errors.New("无效的参数")
	ErrInvalidTime         error = errors.New("无效的时间")
	ErrInvalidTimeFormat   error = errors.New("时间格式不正确")
	ErrInvalidTimestamp    error = errors.New("时间戳不正确")
	ErrInvalidUpload       error = errors.New("无效上传")
	ErrInvalidLimit        error = errors.New("请提供有效的数字")
	ErrInvalidFormat       error = errors.New("格式不正确")
	ErrInvalidPwdFormat    error = errors.New("密码格式不正确")
	ErrInvalidMobileFormat error = errors.New("手机格式不正确")
	ErrInvalidEmailFormat  error = errors.New("Email格式不正确")
	ErrPermissionDenied    error = errors.New("没有权限")
	ErrPwdNotMatch         error = errors.New("密码不一致")
	ErrAccountTaken        error = errors.New("账户已存在")
	ErrLoginFailed         error = errors.New("账号和密码不匹配")
	ErrUserNotFound        error = errors.New("找不到用户")
	ErrNotFound            error = errors.New("找不到资源")
	ErrShouldBeBlank       error = errors.New("必须为空")
	ErrShouldNotBeBlank    error = errors.New("不能为空")
	ErrNoSelection         error = errors.New("请选择一个答案")
	ErrNilData             error = errors.New("空异常")
	ErrAlreadyExist        error = errors.New("已存在")

	ErrShouldNotLessThanZero error = errors.New("不能小于0")
	ErrShouldLargerThanZero  error = errors.New("必须大于0")

	ErrDeviceTaken error = errors.New("该设备已被注册")

	// For Share Request
	ErrInvalidToken error = errors.New("无效的 Token")
	ErrTokenExpired error = errors.New("Token 已过期")

	ErrInvalidEnv      error = errors.New("Runtime Env (dev/prod/test/ci) is not provided")
	ErrServiceNotFound error = errors.New("Service not found")

	// For Category

	ErrMgoNotFound error = mgo.ErrNotFound

	ErrCannotFollowYourSelf error = errors.New("不能关注自己")

	ErrRequireField   error = errors.New("必填项")
	ErrRequireSelect  error = errors.New("必选项")
	ErrIllegalRequest error = errors.New("非法请求")

	ErrInvalidProofCode error = errors.New("无效的验证码")
)

func BuildNotFoundError(str string) error {
	return errors.New(fmt.Sprintf("找不到 %s", str))
}

func BuildShouldNotBeBlankError(str string) error {
	return errors.New(fmt.Sprintf("%s%s", str, ErrShouldNotBeBlank.Error()))
}

func BuildShouldNotLessThan(str string) error {
	return errors.New(fmt.Sprintf("不能小于%s", str))
}
