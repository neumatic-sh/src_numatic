package inouts

import (
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"fmt"
//	"strings"

	"bitbucket.org/neumatic-sh/goutils/validutil"
)

type CaseInput struct {
	validutil.ValidatorErr `,inline`
	//张吉元修改
	FamilyName             uint   `json:"familyName" form:"familyName"`
/*******************************************张吉元注释
  	FamilyName             string  `json:"familyName" form:"familyName"`
	GivenName              string   `json:"givenName" form:"givenName"`
	FamilyPinyin           []string `json:"familyPinyin" form:"familyPinyin"`
	GivenPinyin            []string `json:"givenPinyin" form:"givenPinyin"`
*/
}


func (this *CaseInput) ValidateCreate() uint {
	/*****************************************张吉元注释
	  	var (
	          familyPinyin = string
	 // 		givenPinyin  = []string{}
	  	)
	*/
	//张吉元增加
	    fmt.Printf("这里是输入的原姓氏信息%d\n", this.FamilyName)
	 // 	familyPinyin == fmt.Sprint(this.FamilyName)
	//	fmt.Printf("这里是输入的原名字信息%s\n",this.GivenName)
	// Handle Family Pinyin
	/***************************************张吉元注释
//	for _, py := range fmt.Sprint(this.FamilyName) {
	for _, py := range familyPinyin {
		py = strings.TrimSpace(py)

		if py == "" || py == "0"{
			continue
		}

		this.FamilyName = append(this.FamilyName, strings.Title(py))
	}
   //*/

	//张吉元修改
	if fmt.Sprint(this.FamilyName) == "0" {
		this.AddError("familyPinyin", errorutil.ErrShouldNotBeBlank)
	}

	// Handle Given Pinyin
	/********************************************************张吉元注释
	for _, py := range this.GivenPinyin {
		py = strings.TrimSpace(py)
		if py == "" {
			continue
		}

		givenPinyin = append(givenPinyin, strings.Title(py))
	}
	*/
	//	this.GivenPinyin = givenPinyin
	/******************************************************张吉元注释
	if len(this.GivenPinyin) == 0 {
		this.AddError("givenPinyin", errorutil.ErrShouldNotBeBlank)
	}
	*/
	// Handle Family and Given name
	//	this.FamilyName = strings.TrimSpace(this.FamilyName)
	//	this.GivenName = strings.TrimSpace(this.GivenName)
	//	this.ValidField(this.FamilyName, "familyName", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	//	this.ValidField(this.GivenName, "givenName", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	if this.FamilyName <= 0 || this.FamilyName > 99999999 {
		return 999999999
	}
	return this.FamilyName
}

func (this *CaseInput) ValidateUpdate() bool {
	return true
}
