package models

import (
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"github.com/jinzhu/copier"
	"github.com/kobeld/mgowrap"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetOrInitSetting() (setting *Setting, err error) {
	err = mgowrap.Find(nil, &setting)
	if err != nil {
		if err != mgo.ErrNotFound {
			debugutil.PrintStackAndError(err)
			return
		}
	}

	if setting != nil {
		return
	}

	setting = &Setting{
		Sms: SMS{
			Mobile:   configs.AppConf.SmsMobile,
			Disabled: configs.AppConf.SmsDisabled},
	}

	err = mgowrap.Save(setting)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func UpdateSms(input *inouts.SmsInput) (err error) {

	var (
		sms     = &SMS{}
		changer = bson.M{}
	)

	copier.Copy(sms, input)

	changer["$set"] = bson.M{
		"sms": sms,
	}

	err = mgowrap.Update(&Setting{}, bson.M{}, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}
