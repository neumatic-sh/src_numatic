package controllers

import (
	"fmt"
	"net/http"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"github.com/kobeld/mgowrap"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/inouts/validators"
	"bitbucket.org/neumatic-sh/clinical-research/models"

	"github.com/labstack/echo"
	pinyin "github.com/mozillazg/go-pinyin"
)

func GeneratePinyin(c echo.Context) (err error) {
	var (
		name    = c.Param("name")
		pinyins = []string{}
	)

	for _, py := range pinyin.Pinyin(name, pinyin.NewArgs()) {
		if len(py) > 0 {
			pinyins = append(pinyins, strings.Title(py[0]))
		}
	}

	result := map[string]interface{}{
		"pinyin": pinyins,
	}

	return c.JSON(http.StatusOK, result)
}

func ListUsers(c echo.Context) (err error) {

	var (
		allUsers = []*models.User{}
		allOrgs  = []*models.Organization{}
		allCases = []*models.Case{}
	)

	allOrgs, err = models.FindAllOrgs(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	allUsers, err = models.FindAllUsers(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	allCases, err = models.FindAllCases(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	for _, user := range allUsers {

		var (
			countCases, countSubmitCases int
			theOrg                       *models.Organization
		)

		for _, org := range allOrgs {
			if org.Id == user.OrgID {
				theOrg = org
				break
			}
		}

		for _, caze := range allCases {
			if caze.CreatorID == user.Id {
				countCases++
				if caze.IsFinished() {
					countSubmitCases++
				}
			}
		}

		user.RenderData = map[string]interface{}{
			"org":              theOrg,
			"countCases":       countCases,
			"countSubmitCases": countSubmitCases,
		}

	}

	result := map[string]interface{}{
		"Users": allUsers,
	}

	return c.Render(http.StatusOK, "users/list", result)
}

func NewUser(c echo.Context) (err error) {

	var (
		allOrgs = []*models.Organization{}
	)

	allOrgs, err = models.FindAllOrgs(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"User": &models.User{},
		"Orgs": allOrgs,
	}

	return c.Render(http.StatusOK, "users/new", result)
}

func CreateUser(c echo.Context) (err error) {

	var (
		userInput = &inouts.UserInput{}
	)

	if err = c.Bind(userInput); err != nil {
		return
	}

	validators.ValidateUserCreate(userInput)
	if userInput.HasError() {
		webutil.FlashErrors(c, userInput.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, "/users/new")
	}

	_, err = models.CreateUser(userInput)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.Redirect(http.StatusFound, "/users")
}

func DeleteUser(c echo.Context) (err error) {

	err = models.DeleteUserByIdHex(c.Param("id"))
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
	webutil.FlashInfo(c, "success", "删除成功！")
	return
}

func EditUser(c echo.Context) (err error) {
	var (
		user    *models.User
		id      = c.Param("id")
		allOrgs = []*models.Organization{}
	)

	err = mgowrap.FindByIdHex(id, &user)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	allOrgs, err = models.FindAllOrgs(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"User": user,
		"Orgs": allOrgs,
	}

	return c.Render(http.StatusOK, "users/edit", result)
}

func UpdateUser(c echo.Context) (err error) {

	var (
		id    = c.Param("id")
		input = &inouts.UserInput{}
	)

	err = c.Bind(input)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	validators.ValidateUserUpdate(input, id)
	if input.HasError() {
		webutil.FlashErrors(c, input.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, "/users/new")
	}

	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{
			"name":          input.Name,
			"orgid":         goutils.ToObjectIdOrBlank(input.OrgID),
			"mobile":        input.Mobile,
			"issmsdisabled": input.IsSmsDisabled,
			"role.isadmin":  input.IsAdmin,
		}}
	)

	err = mgowrap.Update(&models.User{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
	webutil.FlashInfo(c, "success", "信息修改成功！")
	return c.Redirect(http.StatusFound, fmt.Sprintf("/users/%s/edit", id))
}
