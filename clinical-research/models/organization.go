package models

import (
	"fmt"
)

type Organization struct {
	Base `,inline`
	Name string
	Code string

	RenderData map[string]interface{} `bson:"-"`
}

func (this *Organization) CollectionName() string {
	return "organizations"
}

func (this *Organization) NameWithCode() string {
	return fmt.Sprintf("%s - %s", this.Code, this.Name)
}
