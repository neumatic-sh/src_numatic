package goutils

import (
	"strings"
)

func ToLowerAndTrimSpace(str string) string {
	return strings.ToLower(strings.TrimSpace(str))
}

// 替换匹配位置的字符，保留个数
// 例如: 18512345678 -> 185----6678
// 		张飞飞 -> 张**
func DesensitizeStr(str string, replace rune, startAt, negativeEndAt int) (result string) {

	var (
		out   = []rune(str)
		count = len(out)
		endAt = count + negativeEndAt - 1
	)

	if endAt < 0 || startAt > endAt {
		endAt = startAt + 1
	}

	// println(endAt)

	for i := range out {
		if i >= startAt && i <= endAt {
			out[i] = replace
		}
	}

	return string(out)
}

// 替换匹配位置的字符，不保留个数
// 例如: 贵州省余庆市ABC111 -> 贵州省余庆*
// 		141275435 -> 141*35
func DesensitizeStrAlt(str string, replace rune, startAt, negativeEndAt int) (result string) {

	var (
		out   = []rune(str)
		count = len(out)
		endAt = count + negativeEndAt - 1
	)

	// fmt.Printf("Count: %+v\n", count)

	if endAt < 0 || startAt > endAt {
		endAt = startAt + 1
	}

	// fmt.Printf("Endat: %+v\n", endAt)

	switch {
	case count < startAt+1:
		result = str
	case count >= startAt+1 && count < endAt+1:
		result = string(append(out[0:startAt], replace))
	case count >= endAt+1:
		arr := append(out[0:startAt], replace)
		arr = append(arr, out[endAt+1:]...)
		result = string(arr)
	}

	return
}
