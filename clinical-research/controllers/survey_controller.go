package controllers

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/neumatic-sh/goutils"

	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"bitbucket.org/neumatic-sh/clinical-research/utils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
)

func ShowSurvey(c echo.Context) (err error) {

	var (
		theCase    *models.Case
		org        *models.Organization
		surveys    []*models.SurveyNew
		selected   *models.SurveyNew
		user       = GetUser(c)
		caseId     = c.Param("id")
		surMatchId = c.Param("surveyOrMatchId")
	)

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	err = mgowrap.FindById(theCase.OrgID, &org)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	surveys, err = models.FindAllSurveysWithFieldsByCaseId(theCase.Id)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if len(surveys) > 0 {
		if surMatchId == "" {
			selected = surveys[0]
		} else {
			for _, survey := range surveys {
				if survey.SurveyOrMatchIdHex() == surMatchId {
					selected = survey
					break
				}
			}
		}
	}
	//log.Printf("[Debug] @@@ShowSurvey selected survey: %v\n", selected)

	paramAnswers := webutil.GetFlashParams(c)
	if len(paramAnswers) > 0 {
		selected.Match.SetFieldsValueWithAnswers(paramAnswers)
	}

	var (
		surveyOutput = models.ConvertToSurveyOutput(selected)
	)

	for _, survey := range surveys {
		if survey.MatchId.Hex() == surveyOutput.MatchId &&
			survey.IdHex() != surveyOutput.SurveyId {
			surveyOutput.IsDeletable = true
			break
		}
	}

	result := map[string]interface{}{
		"Case":    theCase,
		"Surveys": surveys,
		"Org":     org,
		"Survey":  surveyOutput,
		"IsAdmin": user.Role.IsAdmin,
	}

	return c.Render(http.StatusOK, "survey/show", result)
}

func ShowSurveyPDF(c echo.Context) (err error) {

	var (
		theCase       *models.Case
		surveys       []*models.SurveyNew
		surveyOutputs []*inouts.SurveyOutput
		caseId        = c.Param("id")
		surMatchId    = c.Param("surveyOrMatchId")
	)

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	surveys, err = models.FindAllSurveysWithFieldsByCaseId(theCase.Id)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	for _, survey := range surveys {
		if surMatchId == "" {
			surveyOutputs = append(surveyOutputs, models.ConvertToSurveyOutput(survey))
		} else {
			if survey.SurveyOrMatchIdHex() == surMatchId {
				surveyOutputs = []*inouts.SurveyOutput{models.ConvertToSurveyOutput(survey)}
				break
			}
		}
	}

	result := map[string]interface{}{
		"Case":    theCase,
		"Surveys": surveyOutputs,
	}

	return c.Render(http.StatusOK, "survey/show_pdf", result)
}

func ShowSurveyTemplate(c echo.Context) (err error) {
	return c.Render(http.StatusOK, "survey/show_template", nil)
}

func DraftSurvey(c echo.Context) (err error) {
	var (
		answers           map[string][]string
		matchId, surveyId bson.ObjectId
		user              = GetUser(c)
		caseId            = c.Param("id")
		surveyOrMatchId   = c.Param("surveyOrMatchId")
		redirectUrl       = fmt.Sprintf("/cases/%s/%s", caseId, surveyOrMatchId)
	)

	answers, err = c.FormParams()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if values := answers["matchId"]; len(values) > 0 {
		matchId = goutils.ToObjectIdOrBlank(values[0])
	}
	delete(answers, "matchId")

	if values := answers["surveyId"]; len(values) > 0 {
		surveyId = goutils.ToObjectIdOrBlank(values[0])
	}
	delete(answers, "surveyId")

	if !surveyId.Valid() {
		// Create a new record
		survey := &models.SurveyNew{
			OrgId:     user.OrgID,
			CaseId:    goutils.ToObjectIdOrBlank(caseId),
			CreatorId: user.Id,
			MatchId:   matchId,
			Answers:   answers,
			Status:    global.MatchDraft,
		}

		err = mgowrap.Save(survey, survey.SetTime)

		surveyId = survey.Id

	} else {
		// Update an old record
		var (
			selector = bson.M{
				"_id": surveyId,
			}
			changer = bson.M{"$set": bson.M{
				"answers":   answers,
				"updatedat": time.Now()},
			}
		)

		err = mgowrap.Update(&models.SurveyNew{}, selector, changer)
	}

	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	redirectUrl = fmt.Sprintf("/cases/%s/%s", caseId, surveyId.Hex())

	webutil.FlashInfo(c, "success", "暂存成功！")
	return c.Redirect(http.StatusFound, redirectUrl)
}

func SaveSurvey(c echo.Context) (err error) {
	var (
		answers           map[string][]string
		matchId, surveyId bson.ObjectId
		match             *models.Match
		user              = GetUser(c)
		caseId            = c.Param("id")
		surveyOrMatchId   = c.Param("surveyOrMatchId")
		redirectUrl       = fmt.Sprintf("/cases/%s/%s", caseId, surveyOrMatchId)
	)
	//log.Printf("[Debug] $$$ caseId: %v, surveyOrMatchId: %v\n", caseId, surveyOrMatchId)

	answers, err = c.FormParams()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
	//log.Printf("[Debug] $$$ answers: %v\n", answers)

	if values := answers["matchId"]; len(values) > 0 {
		matchId = goutils.ToObjectIdOrBlank(values[0])
	}

	// spew.Dump(answers)

	if values := answers["surveyId"]; len(values) > 0 {
		surveyId = goutils.ToObjectIdOrBlank(values[0])
	}

	// Get the match
	//log.Printf("[Debug] $$$ matchId: %v, surveyId: %v\n", matchId, surveyId)

	err = mgowrap.FindById(matchId, &match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	delete(answers, "surveyId")
	delete(answers, "matchId")

	match.SetFieldsValueWithAnswers(answers)

	columns := models.ConvertFieldsAndHandleColumns(match.Fields)
	ve := ValidateSurveyColumns(columns)
	// spew.Dump(ve)
	if ve.HasError() {
		// spew.Dump(ve.ErrorMap)
		webutil.FlashErrors(c, ve.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, redirectUrl)
	}

	if !surveyId.Valid() {
		// Create a new record
		survey := &models.SurveyNew{
			Name:      match.Name,
			OrgId:     user.OrgID,
			CaseId:    goutils.ToObjectIdOrBlank(caseId),
			CreatorId: user.Id,
			MatchId:   matchId,
			Answers:   answers,
			Status:    global.MatchFinish,
		}
		//log.Println("[Debug] @@@ New survey")
		models.CheckAndUpdatePrivacyFields(answers, nil, match)

		err = mgowrap.Save(survey, survey.SetTime)

		surveyId = survey.Id

		// Check if it need to send sms
		if match.SmsKey != "" && !user.IsSmsDisabled {
			err := utils.SendSMS(match.SmsKey, user.Mobile, "")
			debugutil.PrintStackButSwallowError(&err)
		}

	} else {

		var survey *models.SurveyNew
		err = mgowrap.FindById(surveyId, &survey)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}

		//log.Println("[Debug] @@@ Existing survey")
		//models.CheckAndUpdatePrivacyFields((*models.Answers)(&answers), &survey.Answers, match)
		models.CheckAndUpdatePrivacyFields(answers, survey, match)

		// Update an old record
		var (
			selector = bson.M{
				"_id": surveyId,
			}
			changer = bson.M{"$set": bson.M{
				"answers":   answers,
				"status":    global.MatchFinish,
				"updatedat": time.Now()},
			}
		)
		//log.Printf("[Debug] changer: %v\n", changer)
		err = mgowrap.Update(&models.SurveyNew{}, selector, changer)
	}

	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	redirectUrl = fmt.Sprintf("/cases/%s/%s", caseId, surveyId.Hex())

	webutil.FlashInfo(c, "success", "保存成功！")
	return c.Redirect(http.StatusFound, redirectUrl)
}

func DeleteSurvey(c echo.Context) (err error) {
	//log.Printf("@@@ [DeleteSurvey] c: %v\n", c)
	var (
		caseId = c.Param("id")
		//surveyId = c.Param("surveyId")
		surveyId = c.Param("surveyOrMatchId")

		selector = bson.M{
			"_id":    goutils.ToObjectIdOrBlank(surveyId),
			"caseid": goutils.ToObjectIdOrBlank(caseId)}
		changer = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&models.SurveyNew{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.NoContent(http.StatusNoContent)
}

func NewSurvey(c echo.Context) (err error) {
	log.Printf("@@@ [NewSurvey] c: %v\n", c)
	var (
		user        = GetUser(c)
		survey      *models.SurveyNew
		match       *models.Match
		caseId      = c.Param("id")
		matchId     = goutils.ToObjectIdOrBlank(c.Param("matchId"))
		surveyCount = 0
		surveyNames = []string{}
		surveyId    = ""
	)

	// Get the match
	err = mgowrap.FindById(matchId, &match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	surveyCount, err = mgowrap.Count(&models.SurveyNew{}, bson.M{
		"caseid":  goutils.ToObjectIdOrBlank(caseId),
		"matchid": matchId,
	})
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if surveyCount == 0 {
		surveyNames = []string{match.Name, fmt.Sprintf("%s - %d", match.Name, 1)}
	} else {
		surveyNames = append(surveyNames, fmt.Sprintf("%s - %d", match.Name, surveyCount))
	}

	for _, name := range surveyNames {
		survey = &models.SurveyNew{
			Name:      name,
			OrgId:     user.OrgID,
			CaseId:    goutils.ToObjectIdOrBlank(caseId),
			CreatorId: user.Id,
			MatchId:   match.Id,
			Answers:   map[string][]string{},
		}

		err = mgowrap.Save(survey, survey.SetTime)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}

		surveyId = survey.IdHex()
	}

	redirectUrl := fmt.Sprintf("/cases/%s/%s", caseId, surveyId)
	return c.Redirect(http.StatusFound, redirectUrl)
}

type ValidatedErr struct {
	Name  string
	Error string
}

func ValidateSurveyColumns(columns []*inouts.Column) (ve *validutil.ValidatorErr) {

	ve = &validutil.ValidatorErr{}
	for _, column := range columns {
		validateColumn(column, ve)
	}

	return
}

func validateColumn(column *inouts.Column, ve *validutil.ValidatorErr) {
	switch {
	case column.IsRadio(), column.IsCheckbox(), column.IsSelect():
		var hasSelected bool
		for _, option := range column.Options {
			if option.Selected {

				hasSelected = true

				// Option text
				if option.TextName != "" {
					ve.ValidField(option.TextValue, column.Name, "nonzero",
						errorutil.BuildShouldNotBeBlankError("选项输入").Error())
				}

				// Validate embeded columns
				for _, col := range option.EmbedColumns {
					validateColumn(col, ve)
				}

				// Validate cascaded columns
				for _, col := range option.CascadeColumns {
					validateColumn(col, ve)
				}
			}
		}
		if column.IsRequired && !hasSelected {
			ve.AddError(column.Name, errorutil.ErrNoSelection)
		}

	case column.IsDate(), column.IsText(),
		column.IsTextarea(), column.IsNumber():

		if column.IsRequired && (column.EmbedName == "" || len(column.EmbedColumns) != 0) {
			ve.ValidField(column.Value, column.Name, "nonzero", errorutil.ErrShouldNotBeBlank.Error())
		}
	}

	// Validate the Max and Min
	var (
		errorMsg, minMaxTag string
	)

	switch {
	case column.Min != "" && column.Max != "":
		minMaxTag = fmt.Sprintf("min=%s,max=%s", column.Min, column.Max)
		errorMsg = fmt.Sprintf("取值必须大于等于%s，且小于等于%s", column.Min, column.Max)
	case column.Min != "":
		minMaxTag = fmt.Sprintf("min=%s", column.Min)
		errorMsg = fmt.Sprintf("取值必须大于等于%s", column.Min)
	case column.Max != "":
		minMaxTag = fmt.Sprintf("max=%s", column.Max)
		errorMsg = fmt.Sprintf("取值必须小于等于%s", column.Max)
	}

	if minMaxTag != "" {
		value, err := strconv.ParseFloat(column.Value, 64)
		if err != nil {
			ve.AddError(column.Name, errors.New("格式错误，必须输入数字"))
		} else {
			ve.ValidField(value, column.Name, minMaxTag, errorMsg)
		}
	}

	// Validate embeded columns
	for _, col := range column.EmbedColumns {
		validateColumn(col, ve)
	}
}
