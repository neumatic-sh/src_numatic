package utils

import (
	"errors"
	"fmt"
	//"io"
	//"log"
	"os"
	"strconv"

	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/gob"
)

var (
	label = []byte("haoju")
	// crypto/rand.Reader is a good source of entropy for randomizing the
	// encryption function.
	rng = rand.Reader
)

// encryption algorithm:
//	1. Use public key to encrypt (RSA EncryptOAEP) string -> []bytes
//	2. Return hexdecimal string representation of []bytes
//
func Encrypt(plaintext string, pubkeypath string) (string, error) {
	pubkey, err := readPublicKeyRSA(pubkeypath)
	if err != nil {
		return "", err
	}
	//fmt.Printf("Get Public key: %v\n", *pubkey)

	ciphertext, err := rsa.EncryptOAEP(sha256.New(), rng, pubkey, []byte(plaintext), label)
	if err != nil {
		return "", errors.New("Error from encryption: " + err.Error())
	}

	return Bytes2HexString(ciphertext), nil
}

// decryption algorithm:
//	1. Convert fixed length(512) hexdecimal string to []bytes
//	2. Use private key to decrypt (RSA DecryptOAEP) []bytes
//
func Decrypt(ciphertext string, prikeypath string) (string, error) {
	prikey, err := readPrivateKeyRSA(prikeypath)
	if err != nil {
		return "", err
	}

	//fmt.Printf("Get Private key: %v\n", *prikey)
	//fmt.Printf("Get Public key(from private keyfile): %v\n", prikey.PublicKey)

	plaintext, err := rsa.DecryptOAEP(sha256.New(), rng, prikey, HexString2Bytes(ciphertext), label)
	if err != nil {
		return "", errors.New("Error from decryption: " + err.Error())
	}

	return string(plaintext), nil
}

// in: bytes array representation of a string
// output: convert hexdecimal bytes themselvs to string
// Example:
//	in := []byte("1Ab")
//	output: "314162"
func Bytes2HexString(in []byte) string {
	//fmt.Printf("input len=%d\n", len(in))
	return fmt.Sprintf("%x", in)
}

// in: hexdecimal string with fixed length (512)
// output: hexdecimal bytes array
// Example:
//	in := "314162"
//	output: [0x31,0x41,0x62]
func HexString2Bytes(in string) []byte {
	var arr []byte
	for i := 0; i < len(in); i += 2 {
		if b, err := strconv.ParseUint("0x"+in[i:i+2], 0, 8); err != nil {
			fmt.Printf("strconv.ParseInt error: %v(i=%d, ins=%s)\n", err, i, in[i:i+2])
		} else {
			arr = append(arr, byte(b))
		}
	}
	return arr
}

// pubkeyfile: public keyfile path
// prikeyfile: private keyfile path
func GenerateKeyPairRSA(pubkeypath string, prikeypath string) error {
	test2048Key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return errors.New("GenerateKey error: " + err.Error())
	}
	//fmt.Printf("RSA key pair: %v\n", test2048Key)
	//fmt.Printf("PublicKey: %v\n", test2048Key.PublicKey)
	//fmt.Printf("test2048Key.D: %v\n", test2048Key.D)
	//fmt.Printf("test2048Key.Primes: %v\n", test2048Key.Primes)
	//fmt.Printf("test2048Key.Precomputed: %v\n", test2048Key.Precomputed)

	err = writeGob(pubkeypath, test2048Key.PublicKey)
	if err != nil {
		return errors.New("Write public key error: " + err.Error())
	}

	err = writeGob(prikeypath, test2048Key)
	if err != nil {
		return errors.New("Write private key error: " + err.Error())
	}

	return nil
}

func readPublicKeyRSA(pubkeypath string) (*rsa.PublicKey, error) {
	var pubkey = new(rsa.PublicKey)
	err := readGob(pubkeypath, pubkey)
	if err != nil {
		return nil, errors.New("ReadPublicKeyRSA error: " + err.Error())
	}
	return pubkey, nil
}

func readPrivateKeyRSA(prikeypath string) (*rsa.PrivateKey, error) {
	var prikey = new(rsa.PrivateKey)
	err := readGob(prikeypath, prikey)
	if err != nil {
		return nil, errors.New("ReadPrivateKeyRSA error: " + err.Error())
	}
	return prikey, nil
}

//======================================================

func writeGob(filePath string, object interface{}) error {
	file, err := os.Create(filePath)
	if err == nil {
		encoder := gob.NewEncoder(file)
		encoder.Encode(object)
	}
	file.Close()
	return err
}

func readGob(filePath string, object interface{}) error {
	file, err := os.Open(filePath)
	if err == nil {
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(object)
	}
	file.Close()
	return err
}

//====== old ======
/*
const (
	SHIFT = 11
)

// encryption algorithm:
//	take string as array of byte(0-255)
//	1. add shift
//	2. switch bit7-4 with bit3-0
//
func Encrypt(plaintext string) string {
	log.Println("[PRIVACY] encrypt")
	var b []byte
	for i := 0; i < len(plaintext); i++ {
		b = append(b, switchBits(plaintext[i]+byte(SHIFT)))
	}
	return string(b)
}

func Decrypt(ciphertext string) string {
	log.Println("[PRIVACY] decrypt")
	var b []byte
	for i := 0; i < len(ciphertext); i++ {
		b = append(b, switchBits(ciphertext[i])-byte(SHIFT))
	}
	return string(b)
}

func switchBits(c byte) byte {
	var upper byte = c >> 4
	var lower byte = c & 0xf
	return (lower << 4) | upper
}

func Compress(in []byte) []byte {
	var buf bytes.Buffer
	//sHex := []byte("70fd299a75ee8223a1646ca31cdef0a4a27e9318db3ebbe4aa818b264e97dbdba2649ec50c9bbdf98b34ea1093927689e36e7df498c1de86eb94366650d3271f6aca259afd87af4a876f80880f94fa569fcbd3f5b82946faa08fe8e1da83d7e0fe6f3eeddd7422c8d957426d5f84e4dca9c30fa07d87a4e2e502caa9e653c3bfd71121e75953a3a801a78ff7e49a9a3a9baa795330a95cbfbc15dd7008b7e7a0236d1f0b9323f72aadb91ec3f7f6358c2aad7ee24b21653507e76078d4313d57a60ea8dbf6a48bd82684a1e9607d73a30956068406c8f834b70972df642961a31aedbdd914650b68dc0d9d49083bf93c7da5516e290ce74487103f4677cf8d09")
	//zw := gzip.NewWriter(&buf)
	zw := lzw.NewWriter(&buf, lzw.LSB, 8)
	if _, err := zw.Write(in); err != nil {
		//if _, err := zw.Write(sHex); err != nil {
		log.Fatal(err)
	}
	//if err := zw.Flush(); err != nil {
	//	log.Fatal(err)
	//}
	if err := zw.Close(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Compressed ciphertext(len=%d): %s(%x)\n", len(buf.Bytes()), string(buf.Bytes()), buf.Bytes())

	zr := lzw.NewReader(&buf, lzw.LSB, 8)
	//if err != nil {
	//	log.Fatal(err)
	//}

	var rbuf bytes.Buffer

	if _, err := io.Copy(&rbuf, zr); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Decompressed ciphertext(len=%d): %s(%x)\n", len(rbuf.Bytes()), string(rbuf.Bytes()), rbuf.Bytes())

	if err := zr.Close(); err != nil {
		log.Fatal(err)
	}

	return buf.Bytes()
}

// returns whether the given file or directory exists
func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}*/
