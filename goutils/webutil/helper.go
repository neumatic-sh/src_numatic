package webutil

import (
	"fmt"
	"html"
	"html/template"
	"reflect"
)

func FirstOf(args ...interface{}) interface{} {
	for _, val := range args {
		switch val.(type) {
		case nil:
			continue
		case string:
			if val == "" {
				continue
			}
			return val
		default:
			return val
		}
	}
	return nil
}

func SecureIndex(item reflect.Value, index int) reflect.Value {

	v := indirectInterface(item)
	if !v.IsValid() {
		return reflect.Value{}
	}

	var isNil bool
	if v, isNil = indirect(v); isNil {
		return reflect.Value{}
	}

	switch v.Kind() {
	case reflect.Array, reflect.Slice:
		if index < 0 || index >= v.Len() {
			return reflect.Value{}
		}
		v = v.Index(index)
	default:
		return reflect.Value{}
	}

	return v
}

// indirect returns the item at the end of indirection, and a bool to indicate if it's nil.
func indirect(v reflect.Value) (rv reflect.Value, isNil bool) {
	for ; v.Kind() == reflect.Ptr || v.Kind() == reflect.Interface; v = v.Elem() {
		if v.IsNil() {
			return v, true
		}
	}
	return v, false
}

// indirectInterface returns the concrete value in an interface value,
// or else the zero reflect.Value.
// That is, if v represents the interface value x, the result is the same as reflect.ValueOf(x):
// the fact that x was an interface value is forgotten.
func indirectInterface(v reflect.Value) reflect.Value {
	if v.Kind() != reflect.Interface {
		return v
	}
	if v.IsNil() {
		return reflect.Value{}
	}
	return v.Elem()
}

func Option(label string, val interface{}, curVal interface{}) template.HTML {
	selected := ""
	if fmt.Sprintf("%+v", val) == fmt.Sprintf("%+v", curVal) {
		selected = " selected"
	}

	return template.HTML(fmt.Sprintf(`<option value="%s" %s>%s</option>`,
		html.EscapeString(fmt.Sprintf("%v", val)), selected, html.EscapeString(label)))
}

func MakeArray(size int) (arr []int) {
	for index := 0; index < size; index++ {
		arr = append(arr, index)
	}
	return
}
