package models

type Role struct {
	IsAdmin bool
}

func (this *Role) CollectionName() string {
	return "roles"
}
