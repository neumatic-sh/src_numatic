package inouts

import (
	"fmt"
	"html/template"
	"strings"

	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/goutils"
)

type SurveyOutput struct {
	MatchName        string
	MatchId          string
	SurveyId         string
	StatusBadgeClass string
	IsStatusFinished bool
	IsDeletable      bool
	Sections         []*Section
}

func (this *SurveyOutput) SurveyOrMatchIdHex() (id string) {
	if this.SurveyId != "" {
		id = this.SurveyId
	} else if this.MatchId != "" {
		id = this.MatchId
	}
	return
}

type Section struct {
	Rows []*Row
}

func (this *Section) RowsCount() int {
	return len(this.Rows)
}

type Row struct {
	Columns []*Column
}

func (this *Row) ColumnCount() int {
	return len(this.Columns)
}

func (this *Row) ColumnClass(size string) string {
	number := 12
	if this.ColumnCount() > 1 {
		number = number / this.ColumnCount()
	}
	return fmt.Sprintf("col-%s-%d", size, number)
}

type Column struct {
	Type           string
	Label          template.HTML
	Name           string
	Value          string
	ClassName      string
	GroupName      string
	CascadeName    string
	EmbedName      string
	ScoreName      string
	SensitiveName  string
	DatetimePicker string
	Inline         bool
	IsRequired     bool
	Min            string
	Max            string
	Step           string
	Placeholder    string
	Options        []*Option
	EmbedColumns   []*Column
}

type Option struct {
	Label          template.HTML
	Value          string
	Selected       bool
	CascadeName    string
	TextName       string
	TextValue      string
	CascadeColumns []*Column
	EmbedColumns   []*Column
}

func (this *Column) IsText() bool {
	return this.Type == global.FormTextType
}

func (this *Column) IsHeader() bool {
	return this.Type == global.FormHeaderType
}

func (this *Column) IsRadio() bool {
	return this.Type == global.FormRadioType
}

func (this *Column) IsCheckbox() bool {
	return this.Type == global.FormCheckboxType
}

func (this *Column) IsSelect() bool {
	return this.Type == global.FormSelectType
}

func (this *Column) IsNumber() bool {
	return this.Type == global.FormNumberType
}

func (this *Column) IsDate() bool {
	return this.Type == global.FormDateType
}

func (this *Column) IsTextarea() bool {
	return this.Type == global.FormTextAreaType
}

func (this *Column) IsParagraph() bool {
	return this.Type == global.FormParagraphType
}

func (this *Column) IsOptionGroup() bool {
	switch this.Type {
	case global.FormCheckboxType, global.FormSelectType, global.FormRadioType:
		return true
	}

	return false
}

func (this *Column) SensitiveValue() string {
	value := this.Value
	switch this.SensitiveName {
	case global.SensitiveUserName:
		value = goutils.DesensitizeStr(this.Value, '*', 1, -1)
	case global.SensitiveAddress:
		value = goutils.DesensitizeStrAlt(this.Value, '*', 5, 0)
	case global.SensitivePhone:
		value = goutils.DesensitizeStr(this.Value, '-', 3, -4)
	case global.SensitiveEmail:
		arr := strings.Split(this.Value, "@")
		if len(arr) > 1 {
			value = goutils.DesensitizeStr(arr[0], '-', 1, -1)
			value = fmt.Sprintf("%s@%s", value, arr[1])
		}
	case global.SensitiveID:
		value = goutils.DesensitizeStr(this.Value, '-', 3, -4)
	case global.SensitiveCase:
		value = goutils.DesensitizeStrAlt(this.Value, '*', 1, -2)
	case global.SensitiveBed:
		value = goutils.DesensitizeStr(this.Value, '-', 0, -1)
	case global.SensitiveAdmission:
		value = goutils.DesensitizeStrAlt(this.Value, '*', 1, -1)
	}

	return value
}
