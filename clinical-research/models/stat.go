package models

import (
	"fmt"
	//"log"
	"sort"
	"strconv"
	"time"

	"github.com/kobeld/mgowrap"

	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Stat struct {
	Title  string
	Labels []string
	Items  []*StatItem
}

type StatItem struct {
	Label string
	Count int
}

// The "scope" value can be:
//  - "all": "scopeId" should be blank.
//		Get all stat in the system
// 	- "org": "scopeId" as org Id, can not be blank.
//		Get all stat in the that org.
//  - "role": "scopeId" as creator Id, can not be blank.
// 		Get all stat made by the creator

func GetFieldAndSurveysByStat(statName string, scopeId bson.ObjectId, scope string) (field *Field, surveys []*SurveyNew, err error) {

	if statName == "" {
		return
	}

	var (
		match *Match
		query = bson.M{
			"fields.statname": statName,
		}
	)

	// Get related match and field name
	match, err = FindMatch(query)
	if err != nil {
		if err != mgo.ErrNotFound {
			debugutil.PrintStackAndError(err)
		}
		return
	}

	for _, fi := range match.Fields {
		if fi.StatName == statName {
			field = fi
			break
		}
	}

	if field == nil {
		return
	}

	var (
		fieldName = fmt.Sprintf("answers.%s", field.Name)
		selector  = bson.M{
			"matchid":   match.Id,
			fieldName:   bson.M{"$exists": true},
			"status":    global.MatchFinish,
			"deletedat": bson.M{"$exists": false},
		}
	)

	switch scope {
	case global.ScopeStatOrg:
		if scopeId.Valid() {
			selector["orgid"] = scopeId
		}
	case global.ScopeStatUser:
		if scopeId.Valid() {
			selector["creatorid"] = scopeId
		}
	case global.ScopeStatAll:
		// Do nothing
	}

	// Get surveys
	err = mgowrap.FindAll(selector, &surveys)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func GetSurveyStats(statName string, scopeId bson.ObjectId, scope string) (stat *Stat, err error) {

	stat = &Stat{}

	if statName == "" {
		return
	}

	var (
		field   *Field
		surveys []*SurveyNew
	)

	field, surveys, err = GetFieldAndSurveysByStat(statName, scopeId, scope)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if field == nil {
		return
	}

	var valueCountMap = map[string]int{}

LOOP:
	for _, survey := range surveys {
		for key, values := range survey.Answers {
			if key == field.Name {
				for _, value := range values {
					valueCountMap[value] = valueCountMap[value] + 1
				}
				continue LOOP
			}
		}
	}

	// Build the stats
	stat.Title = field.Label

	for _, option := range field.Values {
		stat.Labels = append(stat.Labels, option.Label)
		stat.Items = append(stat.Items, &StatItem{
			Label: option.Label,
			Count: valueCountMap[option.Value],
		})
	}

	return
}

// get age stats from birthday info
func GetAgeStats(statName string, scopeId bson.ObjectId, scope string) (stat *Stat, err error) {

	stat = &Stat{}

	if statName == "" {
		return
	}

	var (
		field     *Field
		surveys   []*SurveyNew
		birthdays []time.Time
	)

	field, surveys, err = GetFieldAndSurveysByStat(statName, scopeId, scope)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if field == nil {
		return
	}

LOOP:
	for _, survey := range surveys {
		for key, values := range survey.Answers {
			if key == field.Name {
				for _, value := range values {
					birthday, err := time.Parse(global.TIME_LAYOUT_DATE, value)
					if debugutil.HasErrorAndPrintStack(err) {
						continue
					}
					birthdays = append(birthdays, birthday)
				}
				continue LOOP
			}
		}
	}

	var (
		thisYear      = time.Now().Year()
		valueCountMap = map[string]int{
			"30-39": 0,
			"40-49": 0,
			"50-59": 0,
		}
	)

	for _, birthday := range birthdays {
		var (
			age      = thisYear - birthday.Year()
			roundAge = age / 10 * 10
			ageKey   = fmt.Sprintf("%d-%d", roundAge, roundAge+9)
		)
		valueCountMap[ageKey] = valueCountMap[ageKey] + 1
	}

	sortKeys := make([]string, 0, len(valueCountMap))
	for key := range valueCountMap {
		sortKeys = append(sortKeys, key)
	}
	sort.Strings(sortKeys)

	stat.Title = field.Label
	for _, key := range sortKeys {
		stat.Labels = append(stat.Labels, key)
		stat.Items = append(stat.Items, &StatItem{
			Label: key,
			Count: valueCountMap[key],
		})
	}

	return
}

// get age stats from age input
func GetAgeStats_sh(statName string, scopeId bson.ObjectId, scope string) (stat *Stat, err error) {

	stat = &Stat{}

	if statName == "" {
		return
	}

	var (
		field   *Field
		surveys []*SurveyNew
		ages    []int
	)

	field, surveys, err = GetFieldAndSurveysByStat(statName, scopeId, scope)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if field == nil {
		return
	}
	//log.Printf(">>> [SH] field: %v\n", field)
LOOP:
	for _, survey := range surveys {
		for key, values := range survey.Answers {
			if key == field.Name {
				for _, value := range values {
					age, err := strconv.Atoi(value)
					if debugutil.HasErrorAndPrintStack(err) {
						continue LOOP
					}
					ages = append(ages, age)
				}
				continue LOOP
			}
		}
	}

	var (
		valueCountMap = map[string]int{
			"30-39": 0,
			"40-49": 0,
			"50-59": 0,
			"60-69": 0,
			"70-79": 0,
			"80-89": 0,
			"90-99": 0,
		}
	)

	for _, age := range ages {
		var (
			//age      = thisYear - birthday.Year()
			roundAge = age / 10 * 10
			ageKey   = fmt.Sprintf("%d-%d", roundAge, roundAge+9)
		)
		valueCountMap[ageKey] = valueCountMap[ageKey] + 1
	}

	sortKeys := make([]string, 0, len(valueCountMap))
	for key := range valueCountMap {
		sortKeys = append(sortKeys, key)
	}
	sort.Strings(sortKeys)

	stat.Title = field.Label
	for _, key := range sortKeys {
		stat.Labels = append(stat.Labels, key)
		stat.Items = append(stat.Items, &StatItem{
			Label: key,
			Count: valueCountMap[key],
		})
	}

	return
}

// get ranking stats from GOSE number input
func GetRankingStats(statName string, scopeId bson.ObjectId, scope string) (stat *Stat, err error) {

	stat = &Stat{}

	if statName == "" {
		return
	}

	var (
		field   *Field
		surveys []*SurveyNew
		numbers []int
	)

	field, surveys, err = GetFieldAndSurveysByStat(statName, scopeId, scope)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if field == nil {
		return
	}

	//log.Printf(">>> field: %v\n", field)
LOOP:
	for _, survey := range surveys {
		for key, values := range survey.Answers {
			//log.Printf(">>> key: %s\n", key)
			if key == field.Name {
				for _, value := range values {
					num, err := strconv.Atoi(value)
					if debugutil.HasErrorAndPrintStack(err) {
						continue LOOP
					}
					numbers = append(numbers, num)
				}
				continue LOOP
			}
		}
	}

	var (
		valueCountMap = map[string]int{
			"1": 0,
			"2": 0,
			"3": 0,
			"4": 0,
			"5": 0,
			"6": 0,
			"7": 0,
			"8": 0,
		}
	)

	//log.Printf("@@@ size = %d\n", len(numbers))
	for _, num := range numbers {
		//log.Printf("[Model/Stat] num = %d\n", num)
		key := fmt.Sprintf("%d", num)
		valueCountMap[key] = valueCountMap[key] + 1
	}

	sortKeys := make([]string, 0, len(valueCountMap))
	for key := range valueCountMap {
		sortKeys = append(sortKeys, key)
	}
	sort.Strings(sortKeys)

	stat.Title = field.Label
	for _, key := range sortKeys {
		stat.Labels = append(stat.Labels, key)
		stat.Items = append(stat.Items, &StatItem{
			Label: key,
			Count: valueCountMap[key],
		})
	}

	return
}
