package models

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"github.com/kobeld/mgowrap"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func FindOrgByCode(code string) (org *Organization, err error) {
	err = mgowrap.Find(bson.M{
		"code":      code,
		"deletedat": bson.M{"$exists": false},
	}, &org)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		debugutil.PrintStackAndError(err)
		return
	}
	return
}

func FindOrgByIdHex(idHex string) (org *Organization, err error) {
	var (
		query = bson.M{
			"_id":       goutils.ToObjectIdOrBlank(idHex),
			"deletedat": bson.M{"$exists": false},
		}
	)

	err = mgowrap.Find(query, &org)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		debugutil.PrintStackAndError(err)
		return
	}
	return

}

func HasOrgByCode(code string) (yes bool, err error) {
	return mgowrap.HasAny(&Organization{}, bson.M{
		"code":      code,
		"deletedat": bson.M{"$exists": false},
	})
}

func HasOrgByIdHex(idHex string) (yes bool, err error) {
	return mgowrap.HasAny(&Organization{}, bson.M{
		"_id":       goutils.ToObjectIdOrBlank(idHex),
		"deletedat": bson.M{"$exists": false},
	})
}

func FindAllOrgs(query bson.M) (orgs []*Organization, err error) {

	if query == nil {
		query = bson.M{}
	}

	query["deletedat"] = bson.M{"$exists": false}

	err = mgowrap.FindAll(query, &orgs)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return

}

func CreateOrg(orgInput *inouts.OrgInput) (org *Organization, err error) {

	org = &Organization{
		Name: orgInput.Name,
		Code: orgInput.Code,
	}

	err = mgowrap.Save(org, org.SetTime)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func SoftDeleteOrgByIdHex(id string) (err error) {
	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&Organization{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}
