package inouts

import (
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
)

type SmsInput struct {
	validutil.ValidatorErr `,inline`
	Mobile                 string
	Disabled               bool
}

func (this *SmsInput) ValidateUpdate() bool {
	this.ValidField(this.Mobile, "Mobile", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	return this.HasError()
}
