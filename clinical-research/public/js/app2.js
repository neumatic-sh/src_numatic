!function(a){a.fn.datepicker.dates["zh-CN"]={days:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],daysShort:["周日","周一","周二","周三","周四","周五","周六"],daysMin:["日","一","二","三","四","五","六"],months:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],monthsShort:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],today:"今日",clear:"清除",format:"yyyy年mm月dd日",titleFormat:"yyyy年mm月",weekStart:1}}(jQuery);
document.ApiHost = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');

$(function () {

    $(document).on('paste', 'div[name="label"]', function (e) {
        e.preventDefault();
        var text = (e.originalEvent || e).clipboardData.getData('text/plain');
        window.document.execCommand('insertText', false, text);
    });

    $('#js-case-form input.js-family-name, #js-case-form input.js-given-name').on('change', function () {

        // console.log($(this).data("pinyin"));
        var className = $(this).data("pinyin");
        var name = this.value;
        if (!name.length) {
            fillPinyin([], className);
            return;
        };

        $.ajax({
            type: "GET",
            url: document.ApiHost + "/pinyin/" + name,
        }).done(function (data) {
            if (!data) {
                return;
            }
            fillPinyin(data.pinyin, className);
        }).fail(function (data) {
            // TODO
            console.log(data);
        });
    });

    function fillPinyin(pinyin, className) {
        var pinyinInputs = $('#js-case-form .' + className + ' input');

        if (!pinyin.lenth) {
            pinyinInputs.val("");
        };

        for (var index = 0; index < pinyin.length; index++) {
            $(pinyinInputs[index]).val(pinyin[index]);
        };
    };

    $(".js-delete").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();

        var text = "是否确认删除？";

        if (confirm(text)) {
            var url = $(this).data("url"),
                gotoUrl = $(this).data("goto-url");

            $.ajax({
                type: "DELETE",
                url: url
            }).done(function (data) {
                // TODO: improve this.
                location.reload();
                if (gotoUrl) {
                    window.location = gotoUrl;
                } else {
                    location.reload();
                }
            }).fail(function (data) {
                alert(data.statusText);
            });
        };
    });

    $(".js-reset").on("click", function (e) {
        e.preventDefault();
        e.stopPropagation();

        var text = "是否确认清除重填？";

        if (confirm(text)) {
            var $form = $("#css-survey-section .js-action-form");
            $form.find('input:text, input:password, input:file, select, textarea').val('');
            $form.find('input:radio, input:checkbox')
                .removeAttr('checked').removeAttr('selected');
            // for date and number
            $form.find('input[type=date], input[type=number]').val('');
        };
    });

    // $('.datepicker').datepicker({
    //     language: 'zh-CN',
    //     format: 'yyyy-mm-dd',
    //     autoclose: true,
    //     todayHighlight: true
    // });

    $('.picker-datetime').datetimepicker({
        locale: 'zh-CN',
        format: 'YYYY-MM-DD HH:mm'
    });

    $('.picker-date').datetimepicker({
        locale: 'zh-CN',
        format: 'YYYY-MM-DD'
    });

    $('.picker-time').datetimepicker({
        locale: 'zh-CN',
        format: 'HH:mm'
    });

});

(function () {

    // Js redirection
    $(".js-goto-url").click(function () {
        var url = $(this).data("url");
        window.location = url;
    });

    // Js form summit
    $(".js-action-btn").click(function () {
        var $form = $(".js-action-form"),
            url = $(this).data("url");
        $form.attr("action", url);
        $form.submit();
    });

    // Js form summit alt
    $(".js-action-alt-btn").click(function () {
        var $form = $(".js-action-alt-form"),
            url = $(this).data("url");
        $form.attr("action", url);
        $form.submit();
    });

    // Confirmation box
    $(".js-confirm").click(function () {

        var url = $(this).data("url"),
            title = $(this).data("title") || "确定操作？";

        swal({
            title: title,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "取 消",
            confirmButtonText: "确 定",
            closeOnConfirm: false
        }, function () {

            $.post(url).done(function (data) {
                swal({
                    title: "",
                    type: "success",
                    confirmButtonText: "完 成",
                    closeOnConfirm: true
                }, function () {
                    window.location = data.url;
                });

            });
        });
    });

})();

(function () {
    'use strict';

    $(casesTable);
    $(orgsTable);
    $(usersTable);
    $(matchesTable);

    var ioniconCss = {
        icon: "icon",
        iconColumns: "ion-ios-list-outline",
        iconDown: "ion-chevron-down",
        iconRefresh: "ion-refresh",
        iconSearch: "ion-search",
        iconUp: "ion-chevron-up"
    };

    function matchesTable() {
        if (!$.fn.bootgrid) return;

        var matchListTable = $('#js-match-table').bootgrid({
            css: ioniconCss,
            formatters: {
                commands: function (column, row) {
                    return '<a href="/matches/' + row.id + '/edit" class="btn btn-flat btn-sm btn-info" data-row-id="' + row.id + '"><em class="ion-edit"></em></a>' +
                        '<button type="button" class="stop-click-event js-delete btn btn-flat btn-sm btn-danger" data-row-id="' +
                        row.id + '" data-url="/matches/' + row.id + '"><em class="ion-trash-a"></em></button>';
                }
            },
            labels: {
                search: "快速搜索",
                infos: "当前 {{ctx.start}} - {{ctx.end}}（共 {{ctx.total}} 条）"
            }
        });

        matchListTable.on("click.rs.jquery.bootgrid", function (e, cols, row) {
            if (typeof row != "undefined") {
                window.location.href = "/matches/" + row.id + "/edit";
            }
        }).on("loaded.rs.jquery.bootgrid", function (e, cols, row) {
            $(".stop-click-event").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                var text = "是否确认删除研究表？";

                if (confirm(text)) {
                    var url = $(this).data("url"),
                        rowId = $(this).closest("tr").data("row-id");

                    $.ajax({
                        type: "DELETE",
                        url: document.ApiHost + url
                    }).done(function (data) {
                        // TODO: improve this.
                        location.reload();
                    }).fail(function (data) {
                        alert(data.statusText);
                    });
                };
            });
        });
    };

    function casesTable() {

        if (!$.fn.bootgrid) return;

        var caseListTable = $('#js-case-list').bootgrid({
            caseSensitive: false,
            css: ioniconCss,
            formatters: {
                commands: function (column, row) {
                    return '<a href="/cases/' + row.id + '/edit" class="btn btn-flat btn-sm btn-info" data-row-id="' + row.id + '"><em class="ion-edit"></em></a>' +
                        '<button type="button" class="stop-click-event js-delete btn btn-flat btn-sm btn-danger" data-row-id="' +
                        row.id + '" data-url="/cases/' + row.id + '"><em class="ion-trash-a"></em></button>';


                    // row.id + '" target="_blank"><em class="ion-document-text"></em></a>' +

                    // '<a href="/exportPDF?orgId=' + row.orgId + '&caseId=' + row.id + '" class="btn btn-flat btn-sm btn-success stop-click-export" data-row-id="' +
                                     }
            },
            labels: {
                search: "快速搜索",
                infos: "当前 {{ctx.start}} - {{ctx.end}}（共 {{ctx.total}} 条）"
            }
        });

        caseListTable.on("click.rs.jquery.bootgrid", function (e, cols, row) {
            if (typeof row != "undefined") {
                window.location.href = document.ApiHost + "/cases/" + row.id;
                var url = document.ApiHost + "/cases/" + row.id;
                var win = window.open(url, '_blank');
                win.focus();

                $("tr").removeClass("info");
                $('a[data-row-id="'+ row.id +'"]').closest("tr").addClass("info");
            }
        }).on("loaded.rs.jquery.bootgrid", function (e, cols, row) {
            $(".stop-click-export").click(function (e) {
                e.stopPropagation();
            });

            $(".stop-click-event").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                var text = "是否确认删除病例？";

                if (confirm(text)) {
                    var url = $(this).data("url"),
                        rowId = $(this).closest("tr").data("row-id");

                    $.ajax({
                        type: "DELETE",
                        url: document.ApiHost + url
                    }).done(function (data) {
                        // TODO: improve this.
                        location.reload();
                    }).fail(function (data) {
                        alert(data.statusText);
                    });
                };
            });
        });
    };

    function orgsTable() {

        if (!$.fn.bootgrid) return;

        var caseListTable = $('#js-org-list').bootgrid({
            css: ioniconCss,
            formatters: {
                commands: function (column, row) {
                    return '<button type="button" class="stop-click-event js-delete btn btn-flat btn-sm btn-danger" data-row-id="' +
                        row.id + '" data-url="/orgs/' + row.id + '"><em class="ion-trash-a"></em></button>';
                }
            },
            labels: {
                search: "快速搜索",
                infos: "当前 {{ctx.start}} - {{ctx.end}}（共 {{ctx.total}} 条）"
            }
        })

        caseListTable.on("click.rs.jquery.bootgrid", function (e, cols, row) {
            if (typeof row != "undefined") {
                window.location.href = document.ApiHost + "/orgs/" + row.id + "/cases";
            }
        }).on("loaded.rs.jquery.bootgrid", function (e, cols, row) {
            $(".stop-click-event").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                var text = "是否确认删除组织？";

                if (confirm(text)) {
                    var url = $(this).data("url"),
                        rowId = $(this).closest("tr").data("row-id");

                    $.ajax({
                        type: "DELETE",
                        url: document.ApiHost + url
                    }).done(function (data) {
                        // TODO: improve this.
                        location.reload();
                    }).fail(function (data) {
                        alert(data.statusText);
                    });
                };
            });
        });
    };

    function usersTable() {

        if (!$.fn.bootgrid) return;

        var caseListTable = $('#js-user-list').bootgrid({
            css: ioniconCss,
            formatters: {

                commands: function (column, row) {
                    return '<a href="/users/' + row.id + '/edit" class="btn btn-flat btn-sm btn-info" data-row-id="' + row.id + '"><em class="ion-edit"></em></a>' +
                        '<button type="button" class="stop-click-event js-delete btn btn-flat btn-sm btn-danger" data-row-id="' +
                        row.id + '" data-url="/users/' + row.id + '"><em class="ion-trash-a"></em></button>';
                }
            },
            labels: {
                search: "快速搜索",
                infos: "当前 {{ctx.start}} - {{ctx.end}}（共 {{ctx.total}} 条）"
            }
        })

        caseListTable.on("click.rs.jquery.bootgrid", function (e, cols, row) {
            if (typeof row != "undefined") {
                // window.location.href = document.ApiHost + "/orgs/" + row.id + "/cases";
            }
        }).on("loaded.rs.jquery.bootgrid", function (e, cols, row) {
            $(".stop-click-event").click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                var text = "是否确认删除组织？";

                if (confirm(text)) {
                    var url = $(this).data("url"),
                        rowId = $(this).closest("tr").data("row-id");

                    $.ajax({
                        type: "DELETE",
                        url: document.ApiHost + url
                    }).done(function (data) {
                        // TODO: improve this.
                        location.reload();
                    }).fail(function (data) {
                        alert(data.statusText);
                    });
                };
            });
        });
    }

})();

(function () {
    'use strict';

    $(handleMatch);
    $(showMatch);

    function showMatch() {
        var data = $('input[name="Fields"]').val();
        $('.js-fb-render-area').formRender({
            formData: data
        });
    };

    function handleMatch() {

        var $fbEditor = $('#js-fb-editor'),
            $fbRender = $('#js-fb-render'),

            options = {
                formData: $('input[name="Fields"]').val(),
                disabledActionButtons: ['data'],
                disableFields: ['autocomplete', 'button', 'hidden', 'file'],
                disabledAttrs: ["access", "subtype", "other", "toggle"],
                onSave: function () {
                    $fbEditor.toggle();
                    $fbRender.toggle();
                    $('.js-fb-render-area', $fbRender).formRender({
                        formData: formBuilder.formData
                    });
                }
            },
            formBuilder = $fbEditor.formBuilder(options);

        $('.js-fb-render-edit', $fbRender).click(function () {
            $fbEditor.toggle();
            $fbRender.toggle();
        });

        $(".js-match-submit").on("click", function () {
            var self = this,
                url = $(self).data("url"),
                name = $('input[name="Name"]').val(),
                smsKey = $('input[name="SmsKey"]').val(),
                smsParams = $('input[name="SmsParams"]').val(),
                enabled = $('input[name="Enabled"]').is(':checked'),
                formData = formBuilder.actions.getData();

            var data = {
                "Name": name,
                "Fields": formData,
                "Enabled": enabled,
                "SmsKey": smsKey,
                "SmsParams": smsParams,
            };

            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: 'json'
            }).done(function (data, textStatus, jqXHR) {
                if (jqXHR.status == 201) {
                    var editUrl = "/matches/" + data.Id + "/edit";
                    window.location.href = editUrl
                } else {
                    alert("保存成功!")
                }
            }).fail(function (data) {
                // TODO
                console.log(data);
            });
        });
    };
})();


(function () {
    'use strict';

    $(FlotCharts);

    var colors = ["blue-500", "red-500", "green-500",
            "cyan-200", "yellow-500", "pink-500", "orange-500",
            "purple-900", "blue-900", "red-900", "green-900"
        ],
        pieOptions = {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0,
                    label: {
                        show: true,
                        radius: 0.8,
                        formatter: function (label, series) {
                            return '<div class="flot-pie-label">' +
                                Math.round(series.percent) +
                                '%</div>';
                        },
                        background: {
                            opacity: 0.8,
                            color: '#222'
                        }
                    }
                }
            }
        };

    function getBarOptions(ticks) {
        return {
            series: {
                bars: {
                    show: true,
                    fill: 1,
                    barWidth: 0.7,
                    lineWidth: 0,
                    align: "center"
                },
                highlightColor: 'rgba(255,255,255,0.2)'
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0,
                color: '#8394a9'
            },
            tooltip: true,
            tooltipOpts: {
                content: function getTooltip(label, x, y) {
                    return y + " 例";
                }
            },
            yaxis: {
                tickDecimals: 0,
                min: 0
            },
            xaxis: {
                tickColor: 'transparent',
                mode: 'categories',
                font: {
                    color: Colors.byName('grey-600')
                },
                ticks: ticks
            },
            legend: {
                show: false
            },
            shadowSize: 0
        };
    }

    function buildPieData(statItems, labels) {
        if (!statItems) {
            return
        }

        var pieData = [];
        for (let i = 0; i < statItems.length; i++) {
            var item = statItems[i],
                label = item.Label;

            if (labels && labels.length > i) {
                label = labels[i]
            }

            pieData.push({
                'label': label + ": " + item.Count,
                'color': Colors.byName(colors[i]),
                'data': item.Count
            })
        };
        return pieData
    };

    function buildBarTicks(statItems, labels) {
        if (!statItems) {
            return
        }
        var ticks = [];
        for (let i = 0; i < statItems.length; i++) {
            var item = statItems[i],
                label = item.Label;

            if (labels && labels.length > i) {
                label = labels[i]
            }

            ticks.push([
                [i], label
            ])
        };
        return ticks;
    };

    function buildBarData(statItems, color) {
        if (!statItems) {
            return
        }
        var barData = [];
        for (let i = 0; i < statItems.length; i++) {
            var item = statItems[i];
            if (!color) {
                color = colors[i];
            };
            barData.push({
                data: [
                    [i, item.Count]
                ],
                color: Colors.byName(color)
            })
        };
        return barData;
    };

    function drawPie(selector, stat, labels) {
        if (!stat || !stat.Items) {
            return
        }

        var data = buildPieData(stat.Items, labels);
        $(selector).plot(data, pieOptions);
    };

    function drawBar(selector, stat, color, labels) {
        if (!stat || !stat.Items) {
            return
        }

        var ticks = buildBarTicks(stat.Items, labels),
            barData = buildBarData(stat.Items, color);

        $(selector).plot(barData, getBarOptions(ticks));
    }

    function FlotCharts() {
        if (!$.fn.plot) return;

        if (!document.Stat) {
            return
        }

        // console.log(document.Stat);

        drawPie("#pie-flotchart", document.Stat.statGender);
        drawPie("#pie-intervention", document.Stat.statItv);
        // 评分
        //var rankLabels = ["完全无症状", "无明显残疾", "轻度残疾",
        //    "中度残疾", "中度严重残疾", "严重残疾"
        //];
        //drawPie("#pie-ranking", document.Stat.statRank, rankLabels);
        drawPie("#pie-ranking", document.Stat.statRank);
        // 出院状况
        //var leaveLabels = ["回家", "专业护理", "住院康复", "死亡", "其他"]; // to override the class item options!
        //drawBar("#pie-leave-hospital", document.Stat.statLeave, "", leaveLabels);
        drawBar("#pie-leave-hospital", document.Stat.statLeave, "");
        // 离开医院去向
        drawBar("#pie-where2go", document.Stat.statWhere2Go, "");
        // 年龄
        drawBar("#js-bar-ages", document.Stat.statAge, "green-400");

        // 生活质量
        var barStackeData = [{
            "label": "描述 1",
            "color": Colors.byName('teal-500'),
            "data": [
                ["运动能力", 3],
                ["自理能力", 2],
                ["日常活动", 7],
                ["疼痛/不适", 4],
                ["焦虑/抑郁", 4],
                ["与过去12个月相比", 2]
            ]
        }, {
            "label": "描述 2",
            "color": Colors.byName('orange-500'),
            "data": [
                ["运动能力", 3],
                ["自理能力", 1],
                ["日常活动", 2],
                ["疼痛/不适", 3],
                ["焦虑/抑郁", 5],
                ["与过去12个月相比", 6]
            ]
        }, {
            "label": "描述 3",
            "color": Colors.byName('blue-500'),
            "data": [
                ["运动能力", 5],
                ["自理能力", 8],
                ["日常活动", 2],
                ["疼痛/不适", 4],
                ["焦虑/抑郁", 2],
                ["与过去12个月相比", 2]
            ]
        }];
        var barStackedOptions = {
            series: {
                stack: true,
                bars: {
                    align: 'center',
                    lineWidth: 0,
                    show: true,
                    barWidth: 0.6,
                    fill: 1
                }
            },
            grid: {
                borderColor: 'rgba(162,162,162,.26)',
                borderWidth: 0,
                hoverable: true,
                backgroundColor: 'transparent'
            },
            tooltip: true,
            tooltipOpts: {
                content: function (label, x, y) {
                    return '（' + label + '）' + x + ' : ' + y;
                }
            },
            xaxis: {
                tickColor: 'transparent',
                font: {
                    color: Colors.byName('grey-600')
                },
                mode: 'categories'
            },
            yaxis: {
                min: 0,
                tickColor: 'rgba(162,162,162,.26)',
                font: {
                    color: Colors.byName('grey-600')
                }
            },
            legend: {
                show: false
            },
            shadowSize: 0
        };

        $('#pie-life-quality').each(function () {
            var $el = $(this);
            if ($el.data('height')) $el.height($el.data('height'));
            $el.plot(barStackeData, barStackedOptions);
        });
    }
})();
