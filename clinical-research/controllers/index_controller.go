package controllers

import (
	"bytes"
	"encoding/csv"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	wkpdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/grokify/html-strip-tags-go"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

func Index(c echo.Context) (err error) {

	var (
		genderStat, ageStat, itvStat *models.Stat
		rankStat, leaveStat          *models.Stat
		curUser                      = GetUser(c)
		scopeId                      = curUser.Id
		scope                        = global.ScopeStatUser
	)

	// Admin can get all the stat
	if curUser.Role.IsAdmin {
		scope = global.ScopeStatAll
	}

	// curOrg, err = GetCurrentOrg(c)
	// if debugutil.HasErrorAndPrintStack(err) {
	// 	return
	// }

	genderStat, err = models.GetSurveyStats(global.StatGender, scopeId, scope)
	debugutil.PrintStackButSwallowError(&err)

	leaveStat, err = models.GetSurveyStats(global.StatLeave, scopeId, scope)
	debugutil.PrintStackButSwallowError(&err)

	itvStat, err = models.GetSurveyStats(global.StatIntervention, scopeId, scope)
	debugutil.PrintStackButSwallowError(&err)

	rankStat, err = models.GetSurveyStats(global.StatRanking, scopeId, scope)
	debugutil.PrintStackButSwallowError(&err)

	ageStat, err = models.GetAgeStats(global.StatAge, scopeId, scope)
	debugutil.PrintStackButSwallowError(&err)

	result := map[string]interface{}{
		"statAge":    ageStat,
		"statGender": genderStat,
		"statLeave":  leaveStat,
		"statRank":   rankStat,
		"statItv":    itvStat,
	}

	// genderStat, err = models.GetSurveyStats(global.StatGender, scopeId, scope)
	// debugutil.PrintStackButSwallowError(&err)

	// leaveStat, err = models.GetSurveyStats(global.StatLeave, scopeId, scope)
	// debugutil.PrintStackButSwallowError(&err)

	// itvStat, err = models.GetSurveyStats(global.StatIntervention, scopeId, scope)
	// debugutil.PrintStackButSwallowError(&err)

	// //rankStat, err = models.GetSurveyStats(global.StatRanking, curOrg.Id)
	// rankStat, err = models.GetRankingStats(global.StatRanking, scopeId, scope)
	// debugutil.PrintStackButSwallowError(&err)

	// //ageStat, err = models.GetAgeStats(global.StatAge, curOrg.Id)
	// // ageStat, err = models.GetAgeStats_sh(global.StatAge, scopeId, scope)
	// // debugutil.PrintStackButSwallowError(&err)

	// where2goStat, err = models.GetSurveyStats(global.StatWhere2Go, scopeId, scope)
	// debugutil.PrintStackButSwallowError(&err)

	// result := map[string]interface{}{
	// 	"statAge":      ageStat,
	// 	"statGender":   genderStat,
	// 	"statLeave":    leaveStat,
	// 	"statRank":     rankStat,
	// 	"statItv":      itvStat,
	// 	"statWhere2Go": where2goStat,
	// }

	return c.Render(http.StatusOK, "index", result)
}

func ShowAI(c echo.Context) (err error) {
	return c.Render(http.StatusOK, "ai/show", nil)
}

func ExportPDF(c echo.Context) (err error) {

	var (
		pdfUrl                          string
		localFileName, downloadFileName string
		theCase                         *models.Case
		org                             *models.Organization

		orgId           = c.QueryParam("orgId")
		caseId          = c.QueryParam("caseId")
		surveyOrMatchId = c.QueryParam("surveyOrMatchId")
	)

	if caseId == "" {
		err = errors.New("Case ID Should Not Be Blank!")
		return
	}

	err = mgowrap.FindByIdHex(caseId, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	org, err = models.FindOrgByIdHex(orgId)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	localFileName = fmt.Sprintf("%v-%v.pdf", caseId, surveyOrMatchId)

	if surveyOrMatchId != "" {

		var (
			surveys      []*models.SurveyNew
			surveyOutput *inouts.SurveyOutput
		)

		surveys, err = models.FindAllSurveysWithFieldsByCaseId(theCase.Id)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}

		for _, survey := range surveys {
			if survey.SurveyOrMatchIdHex() == surveyOrMatchId {
				surveyOutput = models.ConvertToSurveyOutput(survey)
				break
			}
		}

		downloadFileName = FileName("pdf", org, nil, nil, surveyOutput)

		// FIXME: Should change later
		if configs.AppConf.CertFile != "" {
			pdfUrl = fmt.Sprintf("https://research.neumatic.cn%s/cases/%s/%s/pdf", configs.AppConf.HttpPort, caseId, surveyOrMatchId)
		} else {
			pdfUrl = fmt.Sprintf("http://127.0.0.1%s/cases/%s/%s/pdf", configs.AppConf.HttpPort, caseId, surveyOrMatchId)
		}

	} else {
		// FIXME: Should change later
		if configs.AppConf.CertFile != "" {
			pdfUrl = fmt.Sprintf("https://research.neumatic.cn%s/cases/%s/pdf", configs.AppConf.HttpPort, caseId)
		} else {
			pdfUrl = fmt.Sprintf("http://127.0.0.1%s/cases/%s/pdf", configs.AppConf.HttpPort, caseId)
		}

		downloadFileName = FileName("pdf", org, theCase, nil, nil)
	}

	dir, err := os.Getwd()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	pdfg, err := wkpdf.NewPDFGenerator()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
	// Set global options
	pdfg.Dpi.Set(200)
	pdfg.PageWidth.Set(1280)

	// Create a new input page from an URL
	page := wkpdf.NewPage(pdfUrl)
	page.DisableSmartShrinking.Set(true)
	page.ViewportSize.Set("1280x1024")

	// Add to document
	pdfg.AddPage(page)

	// Create PDF document in internal buffer
	err = pdfg.Create()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	pdfPath := fmt.Sprintf("%s/files/pdf/%v", dir, localFileName)

	// Write buffer contents to file on disk
	err = pdfg.WriteFile(pdfPath)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	pdfBytes, err := ioutil.ReadFile(pdfPath)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	fi, err := os.Stat(pdfPath)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
	// get the size
	size := fi.Size()

	c.Response().Header().Set(echo.HeaderContentType, "application/pdf; charset=utf-8")
	c.Response().Header().Set("Content-Disposition", "attachment;filename="+downloadFileName)
	c.Response().Header().Set("Content-Length", fmt.Sprintf("%d", size))
	c.Response().WriteHeader(http.StatusOK)

	c.Response().Write(pdfBytes)

	return
}

func ExportAll(c echo.Context) (err error) {

	var (
		allCases = []*models.Case{}
		matches  = []*models.Match{}
		surveys  = []*models.SurveyNew{}
		headers  = []string{"研究编号", "登记日期"}
		buf      = &bytes.Buffer{}
		writer   = csv.NewWriter(buf)
		adminIds []bson.ObjectId
	)

	for _, idHex := range configs.AppConf.ExcludedAdminIds {
		if bson.IsObjectIdHex(idHex) {
			adminIds = append(adminIds, bson.ObjectIdHex(idHex))
		}
	}

	query := bson.M{"creatorid": bson.M{"$nin": adminIds}}

	// Get all cases
	allCases, err = models.FindAllCases(query)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Get all enabled match
	matches, err = models.FindAllEnabledMatches(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Get all surveys
	surveys, err = models.FindAllSurveys(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	writer.Write([]string{"\xEF\xBB\xBF"})

	// Set headers
	for _, match := range matches {

		var (
			theSurvey = &models.SurveyNew{}
		)

		for _, survey := range surveys {
			if survey.MatchId == match.Id {
				theSurvey = survey
				break
			}
		}

		theSurvey.SetMatchWithAnswers(match)
		surveyOutputForHeaders := models.ConvertToSurveyOutput(theSurvey)

		for _, section := range surveyOutputForHeaders.Sections {
			for _, row := range section.Rows {

				handleColumnsForHeader(row.Columns, &headers)

			}
		}
	}

	writer.Write(headers)

	// Set values
	for _, theCase := range allCases {
		values := []string{theCase.Code, theCase.CreatedAtDateStr()}
		for _, match := range matches {
			var (
				surveyOutput *inouts.SurveyOutput
				theSurvey    = &models.SurveyNew{}
			)

			for _, survey := range surveys {
				if survey.MatchId != match.Id || survey.CaseId != theCase.Id {
					continue
				}

				theSurvey = survey
				break
			}

			theSurvey.SetMatchWithAnswers(match)
			surveyOutput = models.ConvertToSurveyOutput(theSurvey)

			for _, section := range surveyOutput.Sections {
				for _, row := range section.Rows {

					handleColumns(row.Columns, &values)

				}
			}
		}

		writer.Write(values)
	}

	writer.Flush()

	c.Response().Header().Set(echo.HeaderContentType, "text/csv")
	c.Response().Header().Set("Content-Disposition", "attachment;filename="+"all.csv")
	c.Response().WriteHeader(http.StatusOK)

	c.Response().Write(buf.Bytes())

	return
}

func Export(c echo.Context) (err error) {

	var (
		org      *models.Organization
		orgIdHex = c.QueryParam("orgId")
		matchId  = goutils.ToObjectIdOrBlank(c.QueryParam("matchId"))
		surveys  []*models.SurveyNew
		match    *models.Match
	)

	org, err = models.FindOrgByIdHex(orgIdHex)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Get the match
	err = mgowrap.FindById(matchId, &match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	query := bson.M{
		"orgid":     org.Id,
		"matchid":   matchId,
		"status":    global.MatchFinish,
		"deletedat": bson.M{"$exists": false},
	}

	err = mgowrap.FindAll(query, &surveys)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	var (
		surveyOutputs = []*inouts.SurveyOutput{}
		headers       = []string{}

		buf      = &bytes.Buffer{}
		writer   = csv.NewWriter(buf)
		fileName = FileName("csv", org, nil, match, nil)
	)

	writer.Write([]string{"\xEF\xBB\xBF"})

	for _, survey := range surveys {
		survey.SetMatchWithAnswers(match)
		surveyOutputs = append(surveyOutputs, models.ConvertToSurveyOutput(survey))
	}

	for _, surveyOutput := range surveyOutputs {
		for _, section := range surveyOutput.Sections {
			for _, row := range section.Rows {

				handleColumnsForHeader(row.Columns, &headers)

			}
		}
		writer.Write(headers)
		break
	}

	var values []string
	for _, surveyOutput := range surveyOutputs {
		values = []string{}
		for _, section := range surveyOutput.Sections {
			for _, row := range section.Rows {

				handleColumns(row.Columns, &values)

			}
		}
		writer.Write(values)
	}

	writer.Flush()

	c.Response().Header().Set(echo.HeaderContentType, "text/csv")
	c.Response().Header().Set("Content-Disposition", "attachment;filename="+fileName)
	c.Response().WriteHeader(http.StatusOK)

	c.Response().Write(buf.Bytes())

	return
}

func handleColumnsForHeader(columns []*inouts.Column, headers *[]string) {
	for _, column := range columns {
		if column.IsParagraph() && column.EmbedName == "" {
			continue
		}

		*headers = append(*headers, SanitizeString(string(column.Label)))

		if column.EmbedName != "" || len(column.EmbedColumns) > 0 {
			handleColumnsForHeader(column.EmbedColumns, headers)
		}

		for _, option := range column.Options {

			if len(option.CascadeColumns) > 0 {
				handleColumnsForHeader(option.CascadeColumns, headers)
			}
		}
	}

	return
}

func handleColumns(columns []*inouts.Column, values *[]string) {
	for _, column := range columns {

		if column.IsParagraph() && column.EmbedName == "" {
			continue
		}

		if column.IsOptionGroup() {
			optionLabels := []string{}
			for _, option := range column.Options {
				if !option.Selected {
					continue
				}
				optionText := string(option.Label)
				if option.TextName != "" {
					optionText = fmt.Sprintf("%s %s", optionText, option.TextValue)
				}
				optionLabels = append(optionLabels, optionText)

			}
			*values = append(*values, strings.Join(optionLabels, ` | `))

			for _, option := range column.Options {
				handleColumns(option.CascadeColumns, values)
			}

		} else {
			*values = append(*values, column.SensitiveValue())
		}

		handleColumns(column.EmbedColumns, values)
	}
}

func Ping(c echo.Context) (err error) {
	return c.NoContent(http.StatusNoContent)
}

func FileName(fileType string, org *models.Organization, theCase *models.Case, match *models.Match,
	surveyOutput *inouts.SurveyOutput) (name string) {

	if theCase != nil {
		name = fmt.Sprintf("%s-%s.%s", theCase.Code,
			time.Now().Format(global.TIME_LAYOUT_DATE_COMPACT), fileType)
	} else {

		matchName := ""
		if match != nil {
			matchName = match.Name
		} else if surveyOutput != nil {
			matchName = surveyOutput.MatchName
		}

		name = fmt.Sprintf("%s-%s-%s.%s", matchName, org.Code,
			time.Now().Format(global.TIME_LAYOUT_DATE_COMPACT), fileType)
	}

	return
}

func SanitizeString(str string) string {
	return strings.TrimSpace(strings.Replace(strip.StripTags(str), "&nbsp;", " ", -1))
}
