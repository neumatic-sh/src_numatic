package middlewares

import (
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2"
)

func EchoSetUser(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		var (
			err  error
			user *models.User
			id   = webutil.GetSessionValue(c, "id")
		)

		if id == "" {
			return next(c)
		}

		err = mgowrap.FindByIdHex(id, &user)
		if err != nil {
			if err == mgo.ErrNotFound {
				return next(c)
			} else {
				debugutil.PrintStackAndError(err)
				return err
			}
		}

		if !user.IsValid() {
			return next(c)
		}

		c.Set("me", user)
		return next(c)
	}
}

func EchoAuthorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		if me := c.Get("me"); me == nil {
			webutil.UnsetSessionValue(c, "id")
			return echo.ErrUnauthorized
		}

		return next(c)
	}
}
