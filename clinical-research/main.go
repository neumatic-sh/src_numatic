package main

import (
	"flag"
	"log"

	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"bitbucket.org/neumatic-sh/clinical-research/routers"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"github.com/kobeld/mgowrap"
)

var (
	env        = flag.String("env", "dev", "Running Environment")
	configFile = flag.String("config", "", "The path of configuration file")
)

func init() {
	flag.Parse()
}

func main() {
	if *configFile == "" {
		panic("Please provide the configuration file path!")
	}

	serverEngine := webutil.NewServerEngine(configs.AppConf, *env, *configFile)

	dburl := configs.AppConf.DbDail
	// Use Aliyun Mongodb
	if configs.AppConf.IsProd() {
		dburl = "mongodb://root:a6ZK#t#nOU*Akyxh@dds-bp102a132ba82bd4-pub.mongodb.rds.aliyuncs.com:3717/admin"
	}

	// Setup database
	mgowrap.SetupDatbase(dburl, configs.AppConf.DbName)

	// Remove me after implementing the user management
	initDummyOrgAndUser()
	initSetting()

	if configs.AppConf.CertFile != "" {
		serverEngine.RunEchoHTTPS(routers.MakeEchoEngineWithRouter())
	} else {
		serverEngine.RunEcho(routers.MakeEchoEngineWithRouter())
	}

}

func initDummyOrgAndUser() {
	var (
		err  error
		user = &models.User{
			Name:     "张医生",
			Mobile:   "18123456789",
			Password: global.DefaultPassword,
			Role: models.Role{
				IsAdmin: true,
			},
		}
		org = &models.Organization{
			Name: "山东省立医院",
			Code: "M13",
		}
	)

	theOrg, err := models.FindOrgByCode(org.Code)
	if err != nil {
		panic(err)
	}

	if theOrg == nil {
		err = mgowrap.Save(org, org.SetTime)
		if err != nil {
			panic(err)
		}
		theOrg = org
	}

	user.OrgID = theOrg.Id

	hasRegistered, err := models.HasRegistered(user.Mobile)
	if hasRegistered {
		return
	}

	err = models.Singup(user)
	if err != nil {
		panic(err)
	}
}

func initSetting() {
	setting, err := models.GetOrInitSetting()
	if err != nil {
		panic(err)
	}

	configs.AppConf.SmsMobile = setting.Sms.Mobile
	configs.AppConf.SmsDisabled = setting.Sms.Disabled

	log.Printf("[Init] The setting is:\n %+v \n", setting)

	return
}
