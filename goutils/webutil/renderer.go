package webutil

import (
	"html/template"
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"

	"bitbucket.org/neumatic-sh/goutils/debugutil"

	"github.com/gorilla/sessions"

	"github.com/fsnotify/fsnotify"
	"github.com/labstack/echo"
)

var (
	infoKey   = "info"
	paramsKey = "params"
	errorsKey = "errors"
)

type EchoRenderer struct {
	IsWatch    bool
	Folder     string
	funcs      template.FuncMap
	filepaths  []string
	hasChanged bool
	templates  *template.Template
}

func NewEchoRenderer(folder string, isWatch bool, funcs template.FuncMap) *EchoRenderer {

	var (
		err      error
		renderer = &EchoRenderer{
			IsWatch: isWatch,
			Folder:  folder,
			funcs:   funcs,
		}
		goTmpl = template.New("").Funcs(funcs)
	)

	err = filepath.Walk(folder, func(path string, info os.FileInfo, ferr error) error {
		if ferr != nil {
			return ferr
		}

		if strings.HasSuffix(path, ".html") {
			renderer.filepaths = append(renderer.filepaths, path)
		}
		return nil
	})

	if err != nil {
		panic(err)
	}

	if len(renderer.filepaths) > 0 {
		template.Must(goTmpl.ParseFiles(renderer.filepaths...))
	} else {
		log.Printf("====> [WARNING] No html files be found in folder: %s <=====\n", folder)
	}

	if renderer.IsWatch {

		sig := make(chan os.Signal, 1)
		signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)

		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			panic(err)
		}

		err = filepath.Walk(folder, func(path string, info os.FileInfo, ferr error) error {

			if ferr != nil {
				return ferr
			}

			if info.IsDir() {
				log.Printf("=====> [INFO] Start watching file changes in folder: %s <=====\n", path)
				return watcher.Add(path)
			}

			return nil
		})

		if err != nil {
			panic(err)
		}

		go func() {
			for {
				select {
				case <-sig:
					watcher.Close()
					os.Exit(0)
				case <-watcher.Events:
					renderer.hasChanged = true
				}
			}
		}()
	}

	renderer.templates = goTmpl

	return renderer
}

func (this *EchoRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {

	if this.hasChanged && len(this.filepaths) > 0 {
		this.templates.Funcs(this.funcs)
		template.Must(this.templates.ParseFiles(this.filepaths...))
		this.hasChanged = false
	}

	tmpl, _ := this.templates.Clone()

	result, err := buildRenderObject(c, data)
	if debugutil.HasErrorAndPrintStack(err) {
		return tmpl.ExecuteTemplate(w, name, data)
	}

	return tmpl.ExecuteTemplate(w, name, result)

}

func buildRenderObject(c echo.Context, data interface{}) (ro *RenderObject, err error) {

	ro = &RenderObject{
		Me:     c.Get("me"),
		Data:   data,
		Params: make(map[string][]string),
		Info:   make(map[string]string),
		Errors: make(map[string]string),
		others: make(map[string]interface{}),
	}

	var (
		request  = c.Request()
		response = c.Response().Writer
	)

	sess, ok := c.Get(flashName).(*sessions.Session)
	if !ok {
		return
	}

	if params, ok := sess.Values[paramsKey].(map[string][]string); ok {
		delete(sess.Values, paramsKey)
		ro.Params = params
	}

	if params, ok := sess.Values[infoKey].(map[string]string); ok {
		delete(sess.Values, infoKey)
		ro.Info = params
	}

	if params, ok := sess.Values[errorsKey].(map[string]string); ok {
		delete(sess.Values, errorsKey)
		ro.Errors = params
	}

	err = sess.Save(request, response)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}
