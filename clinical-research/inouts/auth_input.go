package inouts

import (
	"fmt"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
)

type AuthInput struct {
	validutil.ValidatorErr `,inline`
	Mobile                 string `json:"mobile" form:"mobile"`
	Password               string `json:"password" form:"password"`
	Code                   string `json:"code" form:"code"`
}

const (
	validator_name_tag     = "nonzero,max=24"
	validator_email_tag    = "regexp=^[0-9a-zA-Z]+@[0-9a-zA-Z]+(\\.[0-9a-zA-Z]+)+$"
	validator_password_tag = "min=6"
	validator_mobile_tag   = `regexp=^1[3|5|7|8|][0-9]{9}$`
	validator_gender_tag   = `min=0,max=2`
)

func (this *AuthInput) ValidateLogin() {
	this.ValidField(this.Mobile, "mobile", validator_mobile_tag, errorutil.ErrInvalidFormat.Error())
	this.ValidField(this.Password, "password", validator_password_tag, fmt.Sprintf("%s, 至少6位", errorutil.ErrInvalidFormat.Error()))
}
