package inouts

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
	"bitbucket.org/neumatic-sh/clinical-research/global"
)

type Match03Input struct {
	validutil.ValidatorErr `,inline`
	PlannedAt              time.Time
	PlannedAtStr           string `json:"plannedAtStr" form:"plannedAtStr"`
	ClinicalCenter         string `json:"clinicalCenter" form:"clinicalCenter"`
	MalformOption          int    `json:"malformOption" form:"malformOption"`
	PlannerOption          int    `json:"plannerOption" form:"plannerOption"`
	IsAVMConfirmed         int    `json:"isAVMConfirmed" form:"isAVMConfirmed"`
	AVMReason              string `json:"AVMReason" form:"AVMReason"`
	IsConsidered           int    `json:"isConsidered" form:"isConsidered"`
	ConsideredReason       string `json:"consideredReason" form:"consideredReason"`
	IsAccepted             int    `json:"isAccepted" form:"isAccepted"`
	SolutionOption         int    `json:"solutionOption" form:"solutionOption"`
	SurgeryOption          int    `json:"surgeryOption" form:"surgeryOption"`
	UnionDesc              string `json:"unionDesc" form:"unionDesc"`
	Isfractionated         int    `json:"isfractionated" form:"isfractionated"`
	FractionatedDesc       string `json:"fractionatedDesc" form:"fractionatedDesc"`
	Summary                string `json:"summary" form:"summary"`
	Status                 global.MatchStatus
}

func (this *Match03Input) ValidateSubmit() bool {

	var (
		twoOptionMap  = BuildOptionMap(1, 2)
		fiveOptionMap = BuildOptionMap(1, 5)
		tenOptionMap  = BuildOptionMap(1, 10)
	)

	this.ValidField(this.ClinicalCenter, "clinicalCenter", "nonzero", errorutil.ErrShouldNotBeBlank.Error())

	if _, ok := twoOptionMap[this.MalformOption]; !ok {
		this.AddError("malformOption", errorutil.ErrNoSelection)
	}

	if _, ok := fiveOptionMap[this.PlannerOption]; !ok {
		this.AddError("plannerOption", errorutil.ErrNoSelection)
	}

	if _, ok := twoOptionMap[this.IsAVMConfirmed]; !ok {
		this.AddError("isAVMConfirmed", errorutil.ErrNoSelection)
	}

	if _, ok := twoOptionMap[this.IsConsidered]; !ok {
		this.AddError("isConsidered", errorutil.ErrNoSelection)
	}

	if _, ok := twoOptionMap[this.IsAccepted]; !ok {
		this.AddError("isAccepted", errorutil.ErrNoSelection)
	}

	if _, ok := twoOptionMap[this.SolutionOption]; !ok {
		this.AddError("solutionOption", errorutil.ErrNoSelection)
	}

	if this.SolutionOption == 2 {
		if _, ok := tenOptionMap[this.SurgeryOption]; !ok {
			this.AddError("surgeryOption", errorutil.ErrNoSelection)
		}

		if this.SurgeryOption == 10 {
			this.ValidField(this.UnionDesc, "unionDesc", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
		}
	}

	if _, ok := twoOptionMap[this.Isfractionated]; !ok {
		this.AddError("isfractionated", errorutil.ErrNoSelection)
	}

	this.ValidField(this.Summary, "summary", "nonzero", errorutil.ErrShouldNotBeBlank.Error())

	this.ValidField(this.PlannedAtStr, "plannedAtStr", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	if this.PlannedAtStr != "" {
		var err error
		this.PlannedAt, err = time.Parse(global.TIME_LAYOUT_DATE, this.PlannedAtStr)
		if err != nil {
			this.AddError("plannedAtStr", errorutil.ErrInvalidTimeFormat)
		}
	}

	return this.HasError()
}
