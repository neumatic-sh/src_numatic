package controllers

import (
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
)

func NewMatchNew(c echo.Context) (err error) {
	result := map[string]interface{}{
		"Match": &models.Match{},
	}

	return c.Render(http.StatusOK, "matches/new", result)
}

func ShowMatch(c echo.Context) (err error) {

	var (
		match *models.Match
		id    = c.Param("id")
	)

	err = mgowrap.FindByIdHex(id, &match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"Match": match,
	}

	return c.Render(http.StatusOK, "matches/show", result)
}

func ListMatches(c echo.Context) (err error) {
	var (
		allMatches = []*models.Match{}
		sortField  = "-updatedat"
	)

	allMatches, err = models.FindAllMatches(nil, sortField)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"Matches": allMatches,
	}

	return c.Render(http.StatusOK, "matches/list", result)
}

func CreateMatch(c echo.Context) (err error) {

	var (
		user  = GetUser(c)
		match = &models.Match{
			CreatorId: user.Id,
		}
	)

	err = c.Bind(match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	match.CheckClassAndSetFields()
	// TODO: validation
	// fmt.Printf("%+v\n", match)

	err = mgowrap.Save(match, match.SetTime)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.JSON(http.StatusCreated, match)
}

func EditMatch(c echo.Context) (err error) {
	var (
		match *models.Match
		id    = c.Param("id")
	)

	err = mgowrap.FindByIdHex(id, &match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"Match": match,
	}

	return c.Render(http.StatusOK, "matches/edit", result)

}

func UpdateMatch(c echo.Context) (err error) {
	var (
		id    = c.Param("id")
		match = &models.Match{}
	)

	err = c.Bind(match)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	match.CheckClassAndSetFields()

	// TODO: validation
	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{
			"updatedat": time.Now(),
			"name":      match.Name,
			"fields":    match.Fields,
			"enabled":   match.Enabled,
			"smskey":    match.SmsKey,
			"smsparams": match.SmsParams,
		}}
	)

	err = mgowrap.Update(&models.Match{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.JSON(http.StatusAccepted, match)
}

func DeleteMatch(c echo.Context) (err error) {
	err = models.DeleteMatchByIdHex(c.Param("id"))
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	webutil.FlashInfo(c, "success", "删除成功！")
	return
}
