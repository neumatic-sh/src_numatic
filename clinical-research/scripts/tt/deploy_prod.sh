set -e

server=app@106.14.114.176
path="$GOPATH/src/bitbucket.org/neumatic-sh/clinical-research"

echo "Build main..."
CGO_ENABLED=0 GOOS=linux go build -o $path/main $path/main.go

echo "Package views..."

cd $path
tar cvzf views.tar.gz views

echo "Copy everything to the server.."

scp $path/main $server:~/research
scp $path/views.tar.gz $server:~/research/clinical-research
scp $path/configs/config.yaml $server:~/research/clinical-research/configs/config.yaml
scp $path/public/js/*.js $server:~/research/clinical-research/public/js
scp $path/public/css/*.css $server:~/research/clinical-research/public/css
scp $path/public/img/icons/*.svg $server:~/research/clinical-research/public/img/icons

scp $path/scripts/tt/onserver_run.sh $server:~/research/

echo "Cleanup..."
rm $path/views.tar.gz
rm $path/main

echo "Run the script on the server..."
ssh $server 'sudo sh /home/app/research/onserver_run.sh'

