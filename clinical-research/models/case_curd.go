package models

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"github.com/kobeld/mgowrap"
	"gopkg.in/mgo.v2/bson"
)

func SoftDeleteCaseByIdHex(id string) (err error) {
	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&Case{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func FindAllCases(query bson.M) (results []*Case, err error) {
	if query == nil {
		query = bson.M{}
	}

	query["deletedat"] = bson.M{"$exists": false}

	err = mgowrap.FindAll(query, &results, "-updatedat")
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}
