package models

import (
	"encoding/json"
	"strings"

	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"gopkg.in/mgo.v2/bson"
)

type Match struct {
	Base      `,inline`
	Name      string
	Enabled   bool
	SmsKey    string
	SmsParams string
	CreatorId bson.ObjectId
	Fields    []*Field
}

func (this *Match) CollectionName() string {
	return "matches"
}

func (this *Match) FieldsToString() string {
	if len(this.Fields) == 0 {
		return `[]`
	}

	b, err := json.Marshal(this.Fields)
	debugutil.PrintStackButSwallowError(&err)

	return string(b)
}

func (this *Match) SetFieldsValueWithAnswers(answers Answers) {

	for _, field := range this.Fields {
		if field == nil {
			continue
		}
		for key, values := range answers {
			if field.Name != key {
				// Set the option-text
				for _, option := range field.Values {
					if option.TextName == key && len(values) > 0 {
						option.TextValue = strings.TrimSpace(values[0])
					}
				}

				continue
			}

			if field.IsOptionGroup() {
				for _, value := range values {
					for _, option := range field.Values {
						if option.Value == value {
							option.Selected = true
						}
					}
				}
			} else {
				if len(values) > 0 {
					field.Value = strings.TrimSpace(values[0])
				}
			}
		}
	}
}

func (this *Match) CheckClassAndSetFields() {

	for _, field := range this.Fields {
		var (
			classArr = strings.Split(field.ClassName, " ")
		)

		for _, cName := range classArr {
			if strings.HasPrefix(cName, global.OptionText) {
				var (
					arr         = strings.Split(cName, "-")
					optionValue = arr[len(arr)-1]
				)

				for _, option := range field.Values {
					if option.Value == optionValue {
						option.TextName = cName
						break
					}
				}
			} else if strings.HasPrefix(cName, global.StatPrefix) {
				field.StatName = cName
			} else if strings.HasPrefix(cName, global.PrivacyPrefix) {
				field.Privacy = true
			}
		}

		// Don't select any option by default
		for _, option := range field.Values {
			option.Selected = false
		}

	}
}

type Field struct {
	Type        string    `json:"type"`
	SubType     string    `json:"subtype"`
	Label       string    `json:"label"`
	ClassName   string    `json:"className"`
	StatName    string    `json:"statName"`
	Description string    `json:"description"`
	Name        string    `json:"name"`
	Value       string    `json:"value"`
	Required    bool      `json:"required"`
	Inline      bool      `json:"inline"`
	Other       bool      `json:"other"`
	Multiple    bool      `json:"multiple"`
	Privacy     bool      `json:"privacy"`
	Min         string    `json:"min"`
	Max         string    `json:"max"`
	Step        string    `json:"step"`
	Values      []*Option `json:"values"`
	Placeholder string    `json:"placeholder"`
}

type Option struct {
	Label     string `json:"label"`
	Value     string `json:"value"`
	Selected  bool   `json:"selected"`
	TextName  string `json:"textName"`
	TextValue string `json:"textValue"`
}

func (this *Field) IsOptionGroup() bool {
	switch this.Type {
	case "checkbox-group", "select", "radio-group", "autocomplete":
		return true
	}

	return false
}

func (this *Field) IsParagraph() bool {
	return this.Type == global.FormParagraphType
}

func (this *Field) IsPrivacy() bool {
	return this.Privacy
}
