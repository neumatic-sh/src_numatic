package models

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/neumatic-sh/goutils"

	"github.com/jinzhu/copier"

	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/utils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"github.com/kobeld/mgowrap"
	"gopkg.in/mgo.v2/bson"
)

func NewSurveyMatch16(surveyId bson.ObjectId) (match *Match16, err error) {

	match = NewMatch16()

	var (
		selector = bson.M{"_id": surveyId}
		changer  = bson.M{"$push": bson.M{
			"match16s": match,
		}}
	)

	err = mgowrap.Update(&Survey{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func SoftDeleteSurveyByCaseIdHex(id string) (err error) {
	var (
		selector = bson.M{"caseid": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&Survey{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func CreateSurvey(theCase *Case) (survey *Survey, err error) {
	survey = &Survey{
		OrgId:     theCase.OrgID,
		CreatorId: theCase.CreatorID,
		CaseId:    theCase.Id,
		Match16s:  []*Match16{NewMatch16()},
	}

	err = mgowrap.Save(survey, survey.SetTime)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Send sms message
	for _, mobile := range strings.Split(configs.AppConf.SmsMobile, " ") {
		if len(mobile) != 11 {
			continue
		}

		err := utils.SendSMS(global.SMSCaseCreate, mobile, "")
		debugutil.PrintStackButSwallowError(&err)
	}

	return
}

func FindSurveyByCaseId(id bson.ObjectId) (survey *Survey, err error) {

	var (
		selector = bson.M{
			"caseid": id,
		}
	)

	err = mgowrap.Find(selector, &survey)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

// func FindAllSurveys(query bson.M, isWithSoftDeleted bool) (results []*Survey, err error) {
// 	if query == nil {
// 		query = bson.M{}
// 	}

// 	if !isWithSoftDeleted {
// 		query["deletedat"] = bson.M{"$exists": false}
// 	}

// 	err = mgowrap.FindAll(query, &results, "-updatedat")
// 	if debugutil.HasErrorAndPrintStack(err) {
// 		return
// 	}

// 	return
// }

func UpdateMatch01(caseId, filledById bson.ObjectId, input *inouts.Match01Input, survey *Survey) (err error) {

	var (
		match01    *Match01
		oldMatch01 = survey.Match01
		selector   = bson.M{"caseid": caseId}
		changer    = bson.M{}
		now        = time.Now()
	)

	match01 = &Match01{
		Gender:       input.Gender,
		Ethnic:       input.Ethnic,
		IDNo:         input.IDNo,
		Address:      input.Address,
		Relative:     input.Relative,
		Relationship: input.Relationship,
		Phone:        input.Phone,
		HospitalNo:   input.HospitalNo,
		Birthday:     input.Birthday,
	}

	match01.Status = input.Status
	match01.UpdatedAt = now
	match01.FilledById = filledById

	if match01.IsStatusDraft() {
		match01.DraftedAt = now
	} else if oldMatch01.CreatedAt.IsZero() && match01.IsStatusFinished() {
		match01.CreatedAt = now
	}

	changer["$set"] = bson.M{
		"match01":   match01,
		"updatedat": now,
	}

	err = mgowrap.Update(&Survey{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func UpdateMatch02(caseId, filledById bson.ObjectId, input *inouts.Match02Input, survey *Survey) (err error) {
	var (
		match    = &Match02{}
		oldMatch = survey.Match02
		selector = bson.M{"caseid": caseId}
		changer  = bson.M{}
		now      = time.Now()
	)

	copier.Copy(match, input)

	match.Status = input.Status
	match.UpdatedAt = now
	match.FilledById = filledById

	if match.IsStatusDraft() {
		match.DraftedAt = now
	} else if oldMatch.CreatedAt.IsZero() && match.IsStatusFinished() {
		match.CreatedAt = now
	}

	changer["$set"] = bson.M{
		"match02":   match,
		"updatedat": now,
	}

	err = mgowrap.Update(&Survey{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Send sms message
	if oldMatch.CreatedAt.IsZero() && match.IsStatusFinished() {
		for _, mobile := range strings.Split(configs.AppConf.SmsMobile, " ") {
			if len(mobile) != 11 {
				continue
			}

			err := utils.SendSMS(global.SMSMatch02, mobile, "")
			debugutil.PrintStackButSwallowError(&err)
		}
	}

	return
}

func UpdateMatch03(caseId, filledById bson.ObjectId, input *inouts.Match03Input, survey *Survey) (err error) {

	var (
		match    = &Match03{}
		oldMatch = survey.Match03
		selector = bson.M{"caseid": caseId}
		changer  = bson.M{}
		now      = time.Now()
	)

	copier.Copy(match, input)

	match.Status = input.Status
	match.UpdatedAt = now
	match.FilledById = filledById

	if match.IsStatusDraft() {
		match.DraftedAt = now
	} else if oldMatch.CreatedAt.IsZero() && match.IsStatusFinished() {
		match.CreatedAt = now
	}

	changer["$set"] = bson.M{
		"match03":   match,
		"updatedat": now,
	}

	err = mgowrap.Update(&Survey{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Send sms message
	if oldMatch.CreatedAt.IsZero() && match.IsStatusFinished() {
		for _, mobile := range strings.Split(configs.AppConf.SmsMobile, " ") {
			if len(mobile) != 11 {
				continue
			}

			err := utils.SendSMS(global.SMSMatch03Callback, mobile, "")
			debugutil.PrintStackButSwallowError(&err)
		}
	}

	return
}

func UpdateMatch16(caseId, filledById bson.ObjectId, input *inouts.Match16Input, survey *Survey) (err error) {

	var (
		match    = &Match16{}
		oldMatch *Match16
		matchId  = goutils.ToObjectIdOrBlank(input.Id)
		selector = bson.M{
			"caseid":       caseId,
			"match16s._id": matchId,
		}
		changer = bson.M{}
		now     = time.Now()
	)

	for _, m := range survey.Match16s {
		if m.Id == matchId {
			oldMatch = m
		}
	}

	if oldMatch == nil {
		err = errorutil.BuildNotFoundError(fmt.Sprintf("%s - %s", caseId.Hex(), matchId.Hex()))
		debugutil.PrintStackAndError(err)
		return
	}

	copier.Copy(match, input)

	match.Id = matchId
	match.SetFields()
	match.Status = input.Status
	match.UpdatedAt = now
	match.FilledById = filledById

	if match.IsStatusDraft() {
		match.DraftedAt = now
	} else if oldMatch.CreatedAt.IsZero() && match.IsStatusFinished() {
		match.CreatedAt = now
	}

	changer["$set"] = bson.M{
		"match16s.$": match,
		"updatedat":  now,
	}

	err = mgowrap.Update(&Survey{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func CountValidGender(orgID bson.ObjectId) (male, female int, err error) {

	var (
		maleSelector = bson.M{
			"match01.status": "finish",
			"match01.gender": 1,
			"deletedat":      bson.M{"$exists": false},
		}
		femaleSelector = bson.M{
			"match01.status": "finish",
			"match01.gender": 2,
		}
	)

	if orgID.Valid() {
		maleSelector["orgid"] = orgID
		femaleSelector["orgid"] = orgID
	}

	male, err = mgowrap.Count(&Survey{}, maleSelector)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	female, err = mgowrap.Count(&Survey{}, femaleSelector)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

// func CountIntervention(orgID bson.ObjectId) (stat map[string]int, err error) {

// 	var (
// 		surveys  = []*Survey{}
// 		selector = bson.M{
// 			"match03.status":         "finish",
// 			"match03.solutionoption": bson.M{"$ne": 0},
// 			"deletedat":              bson.M{"$exists": false},
// 		}
// 	)

// 	if orgID.Valid() {
// 		selector["orgid"] = orgID
// 	}

// 	stat = map[string]int{}

// 	surveys, err = FindAllSurveys(selector, false)
// 	if debugutil.HasErrorAndPrintStack(err) {
// 		return
// 	}

// 	for _, survey := range surveys {
// 		match := survey.Match03

// 		if match.SolutionOption == 1 {
// 			stat["option1"] = stat["option1"] + 1
// 		} else if match.SolutionOption == 2 {
// 			if match.SurgeryOption == 0 {
// 				continue
// 			}
// 			key := fmt.Sprintf("option%d", match.SurgeryOption+1)
// 			stat[key] = stat[key] + 1
// 		}
// 	}

// 	return
// }

// func CountAges(orgID bson.ObjectId) (stat map[int]int, err error) {
// 	var (
// 		now      = time.Now()
// 		surveys  = []*Survey{}
// 		selector = bson.M{
// 			"match01.status":   "finish",
// 			"match01.birthday": bson.M{"$ne": time.Time{}},
// 			"deletedat":        bson.M{"$exists": false},
// 		}
// 	)

// 	if orgID.Valid() {
// 		selector["orgid"] = orgID
// 	}

// 	stat = map[int]int{
// 		3: 0,
// 		4: 0,
// 		5: 0,
// 	}

// 	surveys, err = FindAllSurveys(selector, false)
// 	if debugutil.HasErrorAndPrintStack(err) {
// 		return
// 	}

// 	for _, survey := range surveys {
// 		var (
// 			match = survey.Match01
// 			age   = now.Year() - match.Birthday.Year()
// 			key   = age / 10
// 		)

// 		stat[key] = stat[key] + 1
// 	}

// 	return
// }
