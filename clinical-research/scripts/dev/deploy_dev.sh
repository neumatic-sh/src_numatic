set -e

server=root@139.196.37.203
path="$GOPATH/src/bitbucket.org/neumatic-sh/clinical-research"

# echo "Build main..."
CGO_ENABLED=0 GOOS=linux go build -o $path/main $path/main.go

echo "Package views..."

cd $path
tar cvzf views.tar.gz views

echo "Copy everything to the server.."

scp $path/main $server:~/app
scp $path/views.tar.gz $server:~/app/clinical-research
scp $path/configs/config.yaml $server:~/app/clinical-research/configs/config.yaml
scp $path/public/js/*.js $server:~/app/clinical-research/public/js
scp $path/public/css/*.css $server:~/app/clinical-research/public/css

# scp $path/public/img/icons/*.svg $server:~/app/clinical-research/public/img/icons

scp $path/scripts/dev/onserver_dev_run.sh $server:~/app/

echo "Cleanup..."
rm $path/views.tar.gz
rm $path/main

echo "Run the script on the server..."
ssh $server -- /root/app/onserver_dev_run.sh

