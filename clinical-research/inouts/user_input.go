package inouts

import (
	"strings"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
)

type UserInput struct {
	validutil.ValidatorErr `,inline`
	Name                   string
	Mobile                 string
	OrgID                  string
	IsAdmin                bool
	IsSmsDisabled          bool
}

type PasswordInput struct {
	validutil.ValidatorErr `,inline`
	OldPassword            string
	NewPassword            string
	ConfirmPassword        string
}

func (this *PasswordInput) ValidateResetPassword() bool {

	var (
		str = "6个字符"
	)

	this.OldPassword = strings.TrimSpace(this.OldPassword)
	this.NewPassword = strings.TrimSpace(this.NewPassword)
	this.ConfirmPassword = strings.TrimSpace(this.ConfirmPassword)

	this.ValidField(this.OldPassword, "OldPassword", "min=6", errorutil.BuildShouldNotLessThan(str).Error())
	this.ValidField(this.NewPassword, "NewPassword", "min=6", errorutil.BuildShouldNotLessThan(str).Error())
	this.ValidField(this.ConfirmPassword, "ConfirmPassword", "min=6", errorutil.BuildShouldNotLessThan(str).Error())

	if this.HasError() {
		return true
	}

	if this.NewPassword != this.ConfirmPassword {

		this.AddError("ConfirmPassword", errorutil.ErrPwdNotMatch)
	}

	return this.HasError()
}
