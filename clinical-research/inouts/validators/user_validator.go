package validators

import (
	"strings"

	"github.com/kobeld/mgowrap"

	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
)

const (
	validator_name_tag     = "nonzero,max=24"
	validator_email_tag    = "regexp=^[0-9a-zA-Z]+@[0-9a-zA-Z]+(\\.[0-9a-zA-Z]+)+$"
	validator_password_tag = "min=6"
	validator_mobile_tag   = `regexp=^1[3|5|7|8|][0-9]{9}$`
	validator_gender_tag   = `min=0,max=2`
)

func ValidateUserCreate(input *inouts.UserInput) bool {

	baseValidate(input)

	// Validate the mobile
	if !input.HasErrorOn("Mobile") {
		hasRegistered, err := models.HasRegistered(input.Mobile)
		if debugutil.HasErrorAndPrintStack(err) {
			return false
		}
		if hasRegistered {
			input.AddError("Mobile", errorutil.ErrAlreadyExist)
		}
	}

	return input.HasError()
}

func ValidateUserUpdate(input *inouts.UserInput, userId string) bool {
	baseValidate(input)

	if input.HasError() {
		return false
	}

	var (
		err     error
		curUser *models.User
	)

	err = mgowrap.FindByIdHex(userId, &curUser)
	if debugutil.HasErrorAndPrintStack(err) {
		return false
	}

	// Validate the mobile
	if !input.HasErrorOn("Mobile") && input.Mobile != curUser.Mobile {
		hasRegistered, err := models.HasRegistered(input.Mobile)
		if debugutil.HasErrorAndPrintStack(err) {
			return false
		}
		if hasRegistered {
			input.AddError("Mobile", errorutil.ErrAlreadyExist)
		}
	}

	return input.HasError()
}

func baseValidate(input *inouts.UserInput) {
	input.Name = strings.TrimSpace(input.Name)
	input.Mobile = strings.TrimSpace(input.Mobile)
	input.OrgID = strings.TrimSpace(input.OrgID)

	input.ValidField(input.Name, "Name", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	input.ValidField(input.Mobile, "Mobile", validator_mobile_tag, errorutil.ErrInvalidFormat.Error())
	input.ValidField(input.OrgID, "OrgID", "nonzero", errorutil.ErrShouldNotBeBlank.Error())

	// Validate the org
	if !input.HasErrorOn("OrgId") {
		hasOrg, err := models.HasOrgByIdHex(input.OrgID)
		if debugutil.HasErrorAndPrintStack(err) {
			return
		}
		if !hasOrg {
			input.AddError("OrgId", errorutil.ErrNotFound)
		}
	}
}
