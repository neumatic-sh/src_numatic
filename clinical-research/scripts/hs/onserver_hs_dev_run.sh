#!/bin/sh
set -e

appPath="/home/app/research/clinical-research-sh-dev"
pidfile="/home/app/research/clinical-research-sh-dev/run.pid"
config="config_hs.yaml"

echo "Untar the views package"
cd $appPath
tar xzvf views.tar.gz

if [[ -f $pidfile ]]; then
	pid=$(cat $pidfile);
	if ps -p $pid > /dev/null; then
		echo "Kill old instance with $pid"
		kill -9 $pid; > /dev/null 2>&1;
	fi
fi

cp /home/app/research/main $appPath/

echo "Start new instance"
echo "start server -->" >> $appPath/app.log

nohup ./main -config=$appPath/configs/$config -env=dev >> $appPath/app.log 2>&1 & echo $! > run.pid &

tail -f -n 10 $appPath/app.log


