package models

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils"

	"bitbucket.org/neumatic-sh/clinical-research/global"

	"gopkg.in/mgo.v2/bson"
)

type Survey struct {
	Base      `,inline`
	OrgId     bson.ObjectId
	CreatorId bson.ObjectId
	CaseId    bson.ObjectId

	Match01  Match01
	Match02  Match02
	Match03  Match03
	Match16s []*Match16
}

func (this *Survey) CollectionName() string {
	return "surveys_old"
}

type Match01 struct {
	BaseMatch    `,inline`
	Gender       int // 1: male 2: female
	Birthday     time.Time
	Ethnic       string
	IDNo         string
	Address      string
	Relative     string
	Relationship string
	Phone        []string
	HospitalNo   string
}

func (this *Match01) BirthdayStr() string {
	return goutils.FormatTime(this.Birthday, global.TIME_LAYOUT_DATE)
}

type Match02 struct {
	BaseMatch `,inline`

	InGroupAt      time.Time
	HasDiagnosed   int // 1: yes 2: no
	HasSigned      int
	IsIncurable    int
	IsLessFive     int
	IsDecreased    int
	IsIncorrigible int
	IsClear        int
}

func (this *Match02) InGroupAtStr() string {
	return goutils.FormatTime(this.InGroupAt, global.TIME_LAYOUT_DATE)
}

type Match03 struct {
	BaseMatch        `,inline`
	PlannedAt        time.Time
	ClinicalCenter   string
	MalformOption    int // 动静脉畸形
	PlannerOption    int // 治疗计划制定者
	IsAVMConfirmed   int // AVM外科治疗指证是否明确
	AVMReason        string
	IsConsidered     int // 是否考虑复合外科治疗
	ConsideredReason string
	IsAccepted       int // 患者是否接受治疗风险
	SolutionOption   int // 治疗方案 1.保守/药物治疗 2.外科治疗
	SurgeryOption    int // 外科治疗选项
	UnionDesc        string
	Isfractionated   int // 分次治疗
	FractionatedDesc string
	Summary          string
}

func (this *Match03) PlannedAtStr() string {
	return goutils.FormatTime(this.PlannedAt, global.TIME_LAYOUT_DATE)
}

type Match16 struct {
	BaseMatch        `,inline`
	Id               bson.ObjectId `bson:"_id"`
	ICHDisabled      bool
	ICHScore         int
	GCS              int
	EyeResp          int
	LangResp         int
	BodyResp         int
	HematomaVol      int
	HematomaIn       int
	HematomaFrom     int
	AgeScore         int
	GraebDisabled    bool
	GraebScore       int
	LeftBrain        int
	RightBrain       int
	ThirdBrain       int
	FourthBrain      int
	HuntHessDisabled bool
	HuntHessLevel    int
	FisherDisabled   bool
	FisherLevel      int
}

func NewMatch16() *Match16 {
	return &Match16{
		Id: bson.NewObjectId(),
	}
}

func (this *Match16) SetFields() {
	if this.ICHDisabled {
		this.ICHScore = 0
		this.GCS = 0
		this.EyeResp = 0
		this.LangResp = 0
		this.BodyResp = 0
		this.HematomaVol = 0
		this.HematomaIn = 0
		this.HematomaFrom = 0
		this.AgeScore = 0
	}

	if this.GraebDisabled {
		this.GraebScore = 0
		this.LeftBrain = 0
		this.RightBrain = 0
		this.FourthBrain = 0
		this.ThirdBrain = 0
	}

	if this.HuntHessDisabled {
		this.HuntHessLevel = 0
	}

	if this.FisherDisabled {
		this.FisherLevel = 0
	}
}

type BaseMatch struct {
	Status     global.MatchStatus
	UpdatedAt  time.Time
	CreatedAt  time.Time
	DraftedAt  time.Time
	FilledById bson.ObjectId `bson:",omitempty"`
}

func (this *BaseMatch) IsStatusFinished() bool {
	return this.Status == global.MatchFinish
}

func (this *BaseMatch) IsStatusInit() bool {
	return this.Status == ""
}

func (this *BaseMatch) IsStatusDraft() bool {
	return this.Status == global.MatchDraft
}

func (this *BaseMatch) IsStatusCheck() bool {
	return this.Status == global.MatchCheck
}

func (this *BaseMatch) StatusBadgeClass() string {
	return global.MatchStatusBadgeMap[this.Status]
}
