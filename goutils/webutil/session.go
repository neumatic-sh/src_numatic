package webutil

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
)

var (
	store          *sessions.CookieStore
	sessionName    = "session"
	flashName      = "flash"
	sesNotFoundErr = echo.NewHTTPError(http.StatusInternalServerError, "Session Not Found")
)

func InitStore(key string) {
	if store != nil {
		return
	}
	store = sessions.NewCookieStore([]byte(key))
}

func GetSessionValue(c echo.Context, key string) (value string) {
	sess, ok := c.Get(sessionName).(*sessions.Session)
	if !ok {
		return ""
	}

	value, _ = sess.Values[key].(string)
	return
}

func SetSessionValue(c echo.Context, key, value string) (err error) {
	var (
		request  = c.Request()
		response = c.Response().Writer
	)

	sess, ok := c.Get(sessionName).(*sessions.Session)
	if !ok {
		return sesNotFoundErr
	}

	sess.Values[key] = value
	return sess.Save(request, response)
}

func UnsetSessionValue(c echo.Context, key string) (err error) {
	var (
		request  = c.Request()
		response = c.Response().Writer
	)

	sess, ok := c.Get(sessionName).(*sessions.Session)
	if !ok {
		return sesNotFoundErr
	}

	delete(sess.Values, key)
	return sess.Save(request, response)
}

func SessionAndFlash(key string, theSessionName, theFlashName string) echo.MiddlewareFunc {

	// Reset the default auth session name
	sessionName = theSessionName
	flashName = theFlashName
	InitStore(key)

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {

			var (
				request = c.Request()
			)

			session, err := store.Get(request, sessionName)
			if err != nil {
				return err
			}

			c.Set(sessionName, session)

			flash, err := store.Get(request, flashName)
			if err != nil {
				return err
			}

			c.Set(flashName, flash)

			defer context.Clear(request)
			return next(c)
		}
	}
}
