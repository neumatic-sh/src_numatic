package controllers

import (
	"fmt"
//	"golang.org/x/tools/blog/atom"
	"net/http"
	"time"

	"bitbucket.org/neumatic-sh/goutils"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"bitbucket.org/neumatic-sh/clinical-research/utils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
)

func ListCases(c echo.Context) (err error) {

	var (
		totalCount, finishedCount int
		currentUser               = GetUser(c)
		result                    = map[string]interface{}{}
		allCases                  = []*models.Case{}
		query                     = bson.M{}
	)

	if !currentUser.Role.IsAdmin {
		query["creatorid"] = currentUser.Id
	}

	allCases, err = models.FindAllCases(query)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	totalCount = len(allCases)

	for _, theCase := range allCases {
		if theCase.IsFinished() {
			finishedCount++
		}
	}

	result = map[string]interface{}{
		"Items":         allCases,
		"TotalCount":    totalCount,
		"UnderwayCount": totalCount - finishedCount,
		"FinishedCount": finishedCount,
	}

	return c.Render(http.StatusOK, "cases/list", result)
}

func ListOrgCases(c echo.Context) (err error) {
	var (
		totalCount, finishedCount int
		org                       *models.Organization
		orgId                     = goutils.ToObjectIdOrBlank(c.Param("id"))
		result                    = map[string]interface{}{}
		allCases                  = []*models.Case{}
	)

	err = mgowrap.FindById(orgId, &org)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	allCases, err = models.FindAllCases(bson.M{"orgid": org.Id})
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	totalCount = len(allCases)

	for _, theCase := range allCases {
		if theCase.IsFinished() {
			finishedCount++
		}
	}

	result = map[string]interface{}{
		"Items":         allCases,
		"Org":           org,
		"TotalCount":    totalCount,
		"UnderwayCount": totalCount - finishedCount,
		"FinishedCount": finishedCount,
	}

	return c.Render(http.StatusOK, "cases/list", result)
}

func ShowCase(c echo.Context) (err error) {

	var (
		theCase *models.Case
		id      = c.Param("id")
	)

	err = mgowrap.FindByIdHex(id, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"Case": theCase,
	}

	return c.Render(http.StatusOK, "cases/show", result)
}

func EditCase(c echo.Context) (err error) {
	var (
		theCase *models.Case
		id      = c.Param("id")
	)

	err = mgowrap.FindByIdHex(id, &theCase)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"Case": theCase,
	}

	return c.Render(http.StatusOK, "cases/edit", result)
}

func UpdateCase(c echo.Context) (err error) {

	var (
		id        = c.Param("id")
		caseInput = &inouts.CaseInput{}
	)

	err = c.Bind(caseInput)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if caseInput.ValidateUpdate() {
		webutil.FlashErrors(c, caseInput.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, fmt.Sprintf("/cases/%s/edit", id))
	}

	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{
			"patient.familyname":   caseInput.FamilyName,
		///	"patient.givenname":    caseInput.GivenName,
		///	"patient.familypinyin": caseInput.FamilyPinyin,
		///	"patient.givenpinyin":  caseInput.GivenPinyin,
			"updatedat":            time.Now(),
		}}
	)

	err = mgowrap.Update(&models.Case{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	webutil.FlashInfo(c, "success", "信息修改成功！")
	return c.Redirect(http.StatusFound, fmt.Sprintf("/cases/%s/edit", id))
}

func NewCase_v1(c echo.Context) (err error) {
	result := map[string]interface{}{
		"Case": &models.Case{},
	}

	return c.Render(http.StatusOK, "cases/new", result)
}

func NewCase(c echo.Context) (err error) {

	var (
		currentOrg *models.Organization
		theCase    *models.Case
		// survey      *models.Survey
		currentUser = GetUser(c)
	)

	currentOrg, err = GetCurrentOrg(c)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// Create the Case record
	theCase = &models.Case{
		OrgID:     currentOrg.Id,
		CreatorID: currentUser.Id,
		Status:    global.CaseUnderway,
	}

	err = mgowrap.Save(theCase, theCase.SetTime)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.Redirect(http.StatusFound, fmt.Sprintf("/cases/%s", theCase.IdHex()))
}

func CreateCase(c echo.Context) (err error) {

	var (
		caseInput = &inouts.CaseInput{}
		pd uint
	)

	if err = c.Bind(caseInput); err != nil {
		return
	}

		pd = caseInput.ValidateCreate()
		if pd == 999999999{
			webutil.FlashErrors(c, caseInput.ErrorMap)
			webutil.FlashParams(c)
			return c.Redirect(http.StatusFound, "/cases/new")
		}
		/***********************************张吉元注释
		webutil.FlashErrors(c, caseInput.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, "/cases/new")
	*/



	// Get current org   获取当前组织
	var (
		code       string
		pinyinCode uint
		caseCount  int
		caseNumb   string
		currentOrg *models.Organization
		theCase    *models.Case
		// survey      *models.Survey
		currentUser = GetUser(c)
	)

	currentOrg, err = GetCurrentOrg(c)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
/********************************************张吉元注释
	pinyinCode, err = genPinyinNameCode(caseInput.FamilyPinyin, caseInput.GivenPinyin)
*/
//***********************************************************张吉元新增--创建病例时间
	timeStr := time.Now().Format("2006-01-02 15:04:05")
	fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~当前字符串形式时间",timeStr)
	fmt.Printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%s\n",timeStr)


//*************************************************************张吉元输入的病人编号   2019-09-26
	pinyinCode = genPinyinNameCode(caseInput.FamilyName)
	fmt.Print("这里是显示之前的病人编号：",pinyinCode)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	caseCount, err = mgowrap.Count(&models.Case{}, nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// M13-Z0MM0-0010-M01
	//张吉元注释
	//	code = fmt.Sprintf("%s-%s-%04d-M00", currentOrg.Code, pinyinCode, caseCount+1)
	code = fmt.Sprintf("%08d-%04d", pinyinCode,caseCount+1) //张吉元新增修改
	caseNumb = fmt.Sprintf("%08d",pinyinCode)
	fmt.Printf("#############################################%s\n",caseNumb)
	//	code = pinyinCode
	// Create the Case record
	theCase = &models.Case{
		OrgID:     currentOrg.Id,
		CreatorID: currentUser.Id,
		Code:      code,
		CaseNumb:  caseNumb,
		Status:    global.CaseUnderway,
		Patient: models.Patient{

			FamilyName:   timeStr,   //
		//	GivenName:    ,
		/************************************************* 张吉元注释
		///	FamilyPinyin: caseInput.FamilyPinyin,
		///	GivenPinyin:  caseInput.GivenPinyin,
	 */
		},
	}
	err = mgowrap.Save(theCase, theCase.SetTime)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	// // Create the Survey
	// _, err = models.CreateSurvey(theCase)
	// if debugutil.HasErrorAndPrintStack(err) {
	// 	return
	// }

	// Send sms message
	// for _, mobile := range strings.Split(configs.AppConf.SmsMobile, " ") {
	// 	if len(mobile) != 11 {
	// 		continue
	// 	}

	if !currentUser.IsSmsDisabled {
		err = utils.SendSMS(global.SMSCaseCreate, currentUser.Mobile, "")
		debugutil.PrintStackButSwallowError(&err)
	}

	// }

	return c.Redirect(http.StatusFound, fmt.Sprintf("/cases/%s", theCase.IdHex()))
}

func DeleteCase(c echo.Context) (err error) {

	err = models.SoftDeleteCaseByIdHex(c.Param("id"))
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	err = models.DeleteSurveysByCaseIdHex(c.Param("id"))
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.NoContent(http.StatusNoContent)
}

func genPinyinNameCode(familyPinyin uint) (code uint) {

	fmt.Printf("这里是输入的case_cotroll.go里的病人编号：%d\n",familyPinyin)

	/***********************************************张吉元注释
	 var (
		hasFamily, hasGiven bool
		codes               = []string{"0", "0", "0", "0", "0"}
		step                = 2
	)

	for index, pinyin := range familyPinyin {
		codes[index] = string(pinyin[0])
		hasFamily = true
	}

	for index, pinyin := range givenPinyin {
		codes[index+step] = string(pinyin[0])
		hasGiven = true
	}

	if !hasFamily || !hasGiven {
		err = errors.New("genPinyinNameCode error")
		debugutil.PrintStackAndError(err)
		return
	}

	code = strings.Join(codes, "")
	return
	 */
	code = familyPinyin
	return code
	}

