package inouts

type optionMap map[int]string

func BuildOptionMap(begin, size int) (om optionMap) {
	om = map[int]string{}

	for index := 0; index < size; index++ {
		om[index+begin] = ""
	}

	return
}

func (this optionMap) contain(key int) bool {
	_, ok := this[key]
	return ok
}
