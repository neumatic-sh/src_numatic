package routers

import (
	"errors"
	"fmt"
	"html"
	"html/template"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/controllers"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"bitbucket.org/neumatic-sh/clinical-research/utils/middlewares"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"github.com/kobeld/mgowrap"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var funcMap = map[string]interface{}{
	"tableIndex": func(start, current int) int {
		return start + current
	},
	"firstof":     webutil.FirstOf,
	"secureIndex": webutil.SecureIndex,
	"checkedInt":  checkedInt,
	"option":      option,
	"curOrg":      curOrg,
	"noescape":    noescape,
	"fileName":    controllers.FileName,
	"dict": func(values ...interface{}) (map[string]interface{}, error) {
		if len(values)%2 != 0 {
			return nil, errors.New("invalid dict call")
		}
		dict := make(map[string]interface{}, len(values)/2)
		for i := 0; i < len(values); i += 2 {
			key, ok := values[i].(string)
			if !ok {
				return nil, errors.New("dict keys must be strings")
			}
			dict[key] = values[i+1]
		}
		return dict, nil
	},
}

func MakeEchoEngineWithRouter() *echo.Echo {

	var (
		e        = echo.New()
		renderer = webutil.NewEchoRenderer("views", configs.AppConf.IsWatched, funcMap)
	)

	e.Renderer = renderer
	e.Use(middleware.Recover())
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))

	// Specify a global error handler to deal with errors with business logics
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		if err == echo.ErrUnauthorized {
			webutil.FlashInfo(c, "warning", "请登录")
			c.Redirect(http.StatusFound, "login")
		}
		e.DefaultHTTPErrorHandler(err, c)
	}

	root := e.Group("/")
	{
		root.Use(webutil.SessionAndFlash("NUMATIC", "N_SESSION", "N_FLASH"))
		root.Use(middleware.CORS())
		root.Use(middlewares.EchoSetUser)
		root.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format: "[Echo] ${time_rfc3339} | ${status} ${method} - ${uri} | ${latency_human} | ${tx_bytes} bytes\n",
		}))

		root.GET("ping", controllers.Ping)


		root.GET("", controllers.Index, middlewares.EchoAuthorize)
		root.GET("login", controllers.Login)
		root.GET("protocol", controllers.Protocol)
		root.POST("protocol",controllers.ProtocolAction)
		root.POST("login", controllers.LoginAction)
		root.GET("signup", controllers.Signup)
		root.POST("signup", controllers.SignupAction)
		root.GET("recover", controllers.Recover)
		root.GET("logout", controllers.Logout)

		root.GET("pinyin/:name", controllers.GeneratePinyin, middlewares.EchoAuthorize)

		root.GET("export", controllers.Export, middlewares.EchoAuthorize)
		root.GET("exportAll", controllers.ExportAll, middlewares.EchoAuthorize)
		root.GET("exportPDF", controllers.ExportPDF, middlewares.EchoAuthorize)

		root.GET("ai", controllers.ShowAI, middlewares.EchoAuthorize)

		root.GET("cases/:id/:surveyOrMatchId/pdf", controllers.ShowSurveyPDF)
		root.GET("cases/:id/pdf", controllers.ShowSurveyPDF)

		cases := root.Group("cases", middlewares.EchoAuthorize)
		{

			cases.GET("/survey/template", controllers.ShowSurveyTemplate)

			cases.GET("", controllers.ListCases)
			cases.POST("", controllers.CreateCase)
			cases.GET("/new", controllers.NewCase_v1)
			cases.GET("/:id", controllers.ShowSurvey)
			cases.POST("/:id", controllers.UpdateCase)
			cases.GET("/:id/edit", controllers.EditCase)
			cases.DELETE("/:id", controllers.DeleteCase)

			cases.GET("/:id/:surveyOrMatchId", controllers.ShowSurvey)

			cases.POST("/:id/:surveyOrMatchId", controllers.SaveSurvey)
			cases.POST("/:id/:surveyOrMatchId/draft", controllers.DraftSurvey)

			cases.POST("/:id/:matchId/new", controllers.NewSurvey)

			//cases.DELETE("/:id/:surveyId", controllers.DeleteSurvey)
			cases.DELETE("/:id/:surveyOrMatchId", controllers.DeleteSurvey)

		}

		settings := root.Group("settings", middlewares.EchoAuthorize)
		{
			settings.GET("/sms", controllers.ShowSMS)
			settings.POST("/sms", controllers.UpdateSMS)

			settings.GET("/reset-password", controllers.ShowResetPassword)
			settings.POST("/reset-password", controllers.ResetPassword)
		}

		orgs := root.Group("orgs", middlewares.EchoAuthorize)
		{
			orgs.GET("", controllers.ListOrgs)
			orgs.POST("", controllers.CreateOrg)
			orgs.GET("/new", controllers.NewOrg)
			orgs.DELETE("/:id", controllers.DeleteOrg)
			orgs.GET("/:id/cases", controllers.ListOrgCases)
		}

		users := root.Group("users", middlewares.EchoAuthorize)
		{
			users.GET("", controllers.ListUsers)
			users.POST("", controllers.CreateUser)
			users.GET("/new", controllers.NewUser)
			users.DELETE("/:id", controllers.DeleteUser)
			users.GET("/:id/edit", controllers.EditUser)
			users.POST("/:id", controllers.UpdateUser)
		}

		matches := root.Group("matches", middlewares.EchoAuthorize)
		{
			matches.GET("", controllers.ListMatches)
			matches.POST("", controllers.CreateMatch)
			matches.GET("/new", controllers.NewMatchNew)
			matches.GET("/:id/edit", controllers.EditMatch)
			matches.GET("/:id", controllers.ShowMatch)
			matches.POST("/:id", controllers.UpdateMatch)
			matches.DELETE("/:id", controllers.DeleteMatch)
		}
	}

	e.Static("/assets", "public")

	return e
}

func checkedInt(targetInt int, param string, valueInt int) string {

	if param != "" {
		paramInt, err := strconv.Atoi(param)
		if err == nil {
			if paramInt == targetInt {
				return "checked"
			}
		}
	}

	if targetInt == valueInt {
		return "checked"
	}

	return ""
}

func option(label string, val interface{}, curVal interface{}) template.HTML {
	selected := ""
	if fmt.Sprintf("%+v", val) == fmt.Sprintf("%+v", curVal) {
		selected = " selected"
	}

	return template.HTML(fmt.Sprintf(`<option value="%s" %s>%s</option>`,
		html.EscapeString(fmt.Sprintf("%v", val)), selected, html.EscapeString(label)))
}

func curOrg(orgId bson.ObjectId) (org *models.Organization) {

	err := mgowrap.FindById(orgId, &org)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return org
}

func noescape(str string) template.HTML {
	return template.HTML(str)
}
