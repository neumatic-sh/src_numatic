package webutil

import "encoding/gob"

var (
	errorClass = "has-error"
)

func init() {
	gob.Register(make(map[string][]string))
	gob.Register(make(map[string]string))
}

type RenderObject struct {
	Me     interface{}
	Data   interface{}
	Params map[string][]string
	Errors map[string]string
	Info   map[string]string
	others map[string]interface{}
}

func (this *RenderObject) HasErr() bool {
	return len(this.Errors) > 0
}

func (this *RenderObject) ErrClass() string {
	if this.HasErr() {
		return errorClass
	}
	return ""
}

func (this *RenderObject) Field(name string) *Field {
	field := &Field{
		Name:   name,
		Error:  this.Errors[name],
		Params: this.Params[name],
	}

	if len(field.Params) > 0 {
		field.Param = field.Params[0]
	}

	return field
}

func (this *RenderObject) Set(key string, value interface{}) string {
	this.others[key] = value
	return ""
}

func (this *RenderObject) Get(key string) interface{} {
	return this.others[key]
}

type Field struct {
	Name   string
	Value  string
	Error  string
	Param  string
	Params []string
}

func (this *Field) HasErr() bool {
	return this.Error != ""
}

func (this *Field) ErrClass() string {
	if this.HasErr() {
		return errorClass
	}
	return ""
}

func (this *Field) IndexParams(index int) string {
	if index >= len(this.Params) {
		return ""
	}
	return this.Params[index]
}

func SetErrorClass(class string) {
	if class == "" {
		return
	}

	errorClass = class
}
