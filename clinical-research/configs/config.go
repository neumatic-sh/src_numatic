package configs

import (
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"github.com/316014408/alidayu"
)

var (
	AppConf = new(appConf)
)

type appConf struct {
	SmsMobile        string
	SmsDisabled      bool
	ExcludedAdminIds []string
	webutil.BaseConfig
}

func (this *appConf) GetName() string {
	return "clinical-research"
}

func init() {
	alidayu.AppKey = "23326361"
	alidayu.AppSecret = "685a0349223f85fcbcfb37f65f3301e6"
}
