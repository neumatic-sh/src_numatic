package controllers

import (
	"net/http"

	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"github.com/labstack/echo"
)

func ShowSMS(c echo.Context) (err error) {
	var (
		setting *models.Setting
	)

	setting, err = models.GetOrInitSetting()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	result := map[string]interface{}{
		"Sms": setting.Sms,
	}

	return c.Render(http.StatusOK, "setting/sms", result)
}

func UpdateSMS(c echo.Context) (err error) {

	var (
		smsInput    = &inouts.SmsInput{}
		redirectUrl = "/settings/sms"
	)

	if err = c.Bind(smsInput); err != nil {
		return
	}

	if smsInput.ValidateUpdate() {
		webutil.FlashErrors(c, smsInput.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, redirectUrl)
	}

	err = models.UpdateSms(smsInput)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	configs.AppConf.SmsMobile = smsInput.Mobile
	configs.AppConf.SmsDisabled = smsInput.Disabled

	return c.Redirect(http.StatusFound, redirectUrl)
}

func ShowResetPassword(c echo.Context) (err error) {

	var (
		input = &inouts.PasswordInput{}
	)
	result := map[string]interface{}{
		"Pwd": input,
	}

	return c.Render(http.StatusOK, "setting/reset_password", result)
}

func ResetPassword(c echo.Context) (err error) {

	var (
		user        = GetUser(c)
		input       = &inouts.PasswordInput{}
		redirectUrl = "/settings/reset-password"
	)

	if err = c.Bind(input); err != nil {
		return
	}

	if input.ValidateResetPassword() {
		webutil.FlashErrors(c, input.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, redirectUrl)
	}

	user, err = models.Login(user.Mobile, input.OldPassword)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if user == nil {
		input.AddError("OldPassword", errorutil.ErrLoginFailed)
		webutil.FlashErrors(c, input.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, redirectUrl)
	}

	user.Password = input.NewPassword
	err = models.Singup(user)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	webutil.FlashInfo(c, "success", "修改密码成功！")
	return c.Redirect(http.StatusFound, redirectUrl)
}
