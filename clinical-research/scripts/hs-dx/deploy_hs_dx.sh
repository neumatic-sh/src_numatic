set -e

server=app@106.14.114.176
path="$GOPATH/src/bitbucket.org/neumatic-sh/clinical-research"
target="clinical-research-hs-dx"
config="config_hs_dx.yaml"

echo "Build main..."
CGO_ENABLED=0 GOOS=linux go build -o $path/main $path/main.go

echo "Package views..."

cd $path
tar cvzf views.tar.gz views

echo "Copy everything to the server.."

scp $path/main $server:~/research
scp $path/views.tar.gz $server:~/research/$target
scp $path/configs/$config $server:~/research/$target/configs/$config
scp $path/public/js/*.js $server:~/research/$target/public/js
scp $path/public/css/*.css $server:~/research/$target/public/css
scp $path/public/img/icons/*.svg $server:~/research/$target/public/img/icons

scp $path/scripts/hs-dx/onserver_hs_dx_run.sh $server:~/research/

echo "Cleanup..."
rm $path/views.tar.gz
rm $path/main

echo "Run the script on the server..."
ssh $server 'sudo sh /home/app/research/onserver_hs_dx_run.sh'

