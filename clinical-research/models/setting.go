package models

type Setting struct {
	Base `,inline`
	Sms  SMS
}

type SMS struct {
	Mobile   string
	Disabled bool
}

func (this *Setting) CollectionName() string {
	return "settings"
}
