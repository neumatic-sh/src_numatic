package models

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/mohae/deepcopy"

	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/utils"
	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"github.com/kobeld/mgowrap"
	"gopkg.in/mgo.v2/bson"
)

type Answers map[string][]string

type SurveyNew struct {
	Base      `,inline`
	OrgId     bson.ObjectId
	CreatorId bson.ObjectId
	CaseId    bson.ObjectId

	MatchId bson.ObjectId
	Answers Answers
	Status  global.MatchStatus

	Name  string
	Match *Match `bson:"-"`
}

func (this *SurveyNew) CollectionName() string {
	return "surveys"
}

func (this *SurveyNew) SurveyOrMatchIdHex() string {
	if this.Id.Valid() {
		return this.Id.Hex()
	}

	if this.Match != nil {
		return this.Match.IdHex()
	}
	return ""
}

func (this *SurveyNew) IsStatusFinished() bool {
	return this.Status == global.MatchFinish
}

func (this *SurveyNew) IsStatusInit() bool {
	return this.Status == ""
}

func (this *SurveyNew) IsStatusDraft() bool {
	return this.Status == global.MatchDraft
}

func (this *SurveyNew) IsStatusCheck() bool {
	return this.Status == global.MatchCheck
}

func (this *SurveyNew) StatusBadgeClass() string {
	return global.MatchStatusBadgeMap[this.Status]
}

func (this *SurveyNew) SetMatchWithAnswers(match *Match) {

	var surveyMatch = deepcopy.Copy(match).(*Match)

	surveyMatch.SetFieldsValueWithAnswers(this.Answers)

	// if this.Name == "" {
	// 	this.Name = surveyMatch.Name
	// }

	// Always show the match name
	this.Name = surveyMatch.Name

	this.Match = surveyMatch
}

func FindAllSurveysWithFieldsByCaseId(caseId bson.ObjectId) (result []*SurveyNew, err error) {

	var (
		matches  []*Match
		surveys  []*SurveyNew
		selector = bson.M{
			"caseid":    caseId,
			"deletedat": bson.M{"$exists": false},
		}
	)

	err = mgowrap.FindAll(selector, &surveys)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	matches, err = FindAllEnabledMatches(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	for _, match := range matches {
		var hasSurvey bool
		for _, survey := range surveys {
			// There may be multiple surveys attached to the same match
			if survey.MatchId == match.Id {
				survey.SetMatchWithAnswers(match)
				result = append(result, survey)
				hasSurvey = true
			}
		}
		if !hasSurvey {
			var survey = &SurveyNew{}
			survey.SetMatchWithAnswers(match)
			result = append(result, survey)
		}
	}

	return
}

func FindAllSurveys(query bson.M, sortFields ...string) (surveys []*SurveyNew, err error) {

	if query == nil {
		query = bson.M{}
	}

	query["deletedat"] = bson.M{"$exists": false}

	err = mgowrap.FindAll(query, &surveys, sortFields...)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func TransformInputToAnswers(input map[string]interface{}) Answers {

	result := map[string][]string{}

	for name, value := range input {
		switch v := value.(type) {
		case string:
			result[name] = []string{v}
		case []interface{}:
			strArr := []string{}
			for _, i := range v {
				strArr = append(strArr, i.(string))
			}
			result[name] = strArr
		}
	}
	return result
}

func ConvertToSurveyOutput(survey *SurveyNew) *inouts.SurveyOutput {

	if survey == nil || survey.Match == nil {
		return &inouts.SurveyOutput{}
	}

	var (
		groupColumns = ConvertFieldsToGroupColumns(survey.Match.Fields)
		match        = survey.Match
		output       = &inouts.SurveyOutput{
			MatchName:        survey.Name,
			MatchId:          match.IdHex(),
			SurveyId:         survey.IdHex(),
			StatusBadgeClass: survey.StatusBadgeClass(),
			IsStatusFinished: survey.IsStatusFinished(),
		}
	)

	if output.MatchName == "" {
		output.MatchName = match.Name
	}

	for _, groupColumn := range groupColumns {

		var (
			row = &inouts.Row{
				Columns: *groupColumn,
			}
			section = &inouts.Section{
				Rows: []*inouts.Row{row},
			}
		)
		output.Sections = append(output.Sections, section)
	}

	return output
}

func ConvertFieldsToGroupColumns(fields []*Field) []*[]*inouts.Column {
	columns := ConvertFieldsToColumns(fields)
	columns = HandleEmbedColumns(columns)
	columns = HandleCascadeColumns(columns)
	// spew.Dump(columns)
	return HandleGroupRowColumns(columns)
}

func ConvertFieldsAndHandleColumns(fields []*Field) (columns []*inouts.Column) {
	columns = ConvertFieldsToColumns(fields)
	columns = HandleEmbedColumns(columns)
	columns = HandleCascadeColumns(columns)

	// spew.Dump(columns)
	return
}

func ConvertFieldsToColumns(fields []*Field) (columns []*inouts.Column) {

	columns = []*inouts.Column{}

	for _, field := range fields {

		// Check special class
		var (
			groupName      string
			cascadeName    string
			embedName      string
			scoreName      string
			sensitiveName  string
			min            string
			max            string
			datetimePicker string
		)

		// Set the default datetime picker class name
		if field.Type == global.FormDateType {
			datetimePicker = "picker-datetime"
		}

		classArr := strings.Split(field.ClassName, " ")
		for _, cName := range classArr {

			if strings.HasPrefix(cName, global.GroupRowPrefix) {
				groupName = cName
			} else if strings.HasPrefix(cName, global.CascadePrefix) {
				cascadeName = cName
			} else if strings.HasPrefix(cName, global.EmbedPrefix) {
				embedName = cName
			} else if strings.HasPrefix(cName, global.ScorePrefix) {
				scoreName = cName
			} else if strings.HasPrefix(cName, global.SensitivePrefix) {
				sensitiveName = cName
			} else if strings.HasPrefix(cName, global.ValidateMinPrefix) {

				minTmp := strings.TrimLeft(cName, global.ValidateMinPrefix)
				if _, err := strconv.Atoi(minTmp); err == nil {
					min = minTmp
				}

			} else if strings.HasPrefix(cName, global.ValidateMaxPrefix) {
				// spew.Dump(cName)
				maxTmp := strings.TrimLeft(cName, global.ValidateMaxPrefix)
				if _, err := strconv.Atoi(maxTmp); err == nil {
					max = maxTmp
				}

			} else if strings.HasPrefix(cName, global.Picker) {
				datetimePicker = cName
			}

		}

		var column = &inouts.Column{
			Type:           field.Type,
			Name:           field.Name,
			Label:          filterLabel(field.Label),
			Value:          field.Value,
			Inline:         field.Inline,
			ClassName:      field.ClassName,
			GroupName:      groupName,
			CascadeName:    cascadeName,
			EmbedName:      embedName,
			ScoreName:      scoreName,
			SensitiveName:  sensitiveName,
			DatetimePicker: datetimePicker,
			Placeholder:    field.Placeholder,
			IsRequired:     field.Required,
			Min:            min,
			Max:            max,
			Step:           field.Step,
			EmbedColumns:   []*inouts.Column{},
		}

		// if min != "" || max != "" {
		// 	spew.Dump(column)
		// }

		for _, option := range field.Values {
			theOption := &inouts.Option{
				Label:        filterLabel(option.Label),
				Value:        option.Value,
				Selected:     option.Selected,
				TextName:     option.TextName,
				TextValue:    option.TextValue,
				EmbedColumns: []*inouts.Column{},
			}

			if cascadeName != "" {
				theOption.CascadeName = fmt.Sprintf("%s-%s", cascadeName, theOption.Value)
				theOption.CascadeColumns = []*inouts.Column{}
			}
			column.Options = append(column.Options, theOption)
		}

		columns = append(columns, column)

	}
	return
}

func HandleEmbedColumns(beforeCols []*inouts.Column) (afterCols []*inouts.Column) {

	afterCols = []*inouts.Column{}
	embedCols := []*inouts.Column{}

	for _, col := range beforeCols {
		if col.EmbedName == "" {
			afterCols = append(afterCols, col)
			continue
		}

		// The convention for embed class is:
		// 	parent: embed-{name}
		//	children: embed-{name}-{other}
		var (
			// parentCol *inouts.Column
			strs = strings.Split(col.EmbedName, "-")
		)

		// Is a parent and other cases
		if len(strs) <= 2 {
			afterCols = append(afterCols, col)
			// continue
		} else {
			embedCols = append(embedCols, col)
		}
	}

	for _, topCol := range afterCols {

		if topCol.EmbedName == "" {
			continue
		}

		for _, embedCol := range embedCols {

			switch {
			case topCol.IsParagraph():
				if strings.Contains(fmt.Sprintf("%s", topCol.Label), embedCol.EmbedName) {
					topCol.EmbedColumns = append(topCol.EmbedColumns, embedCol)
				}
			case topCol.IsCheckbox() || topCol.IsRadio():
				for _, option := range topCol.Options {
					if strings.Contains(string(option.Label), embedCol.EmbedName) {
						option.EmbedColumns = append(option.EmbedColumns, embedCol)
					}
				}
			}

		}
	}

	return
}

func HandleCascadeColumns(beforeCols []*inouts.Column) (afterCols []*inouts.Column) {

	afterCols = []*inouts.Column{}

LOOP:
	for _, curCol := range beforeCols {
		if curCol.CascadeName == "" {
			afterCols = append(afterCols, curCol)
			continue
		}

		// The convention for embed class is:
		// 	parent: cascade-{name}
		//	children: cascade-{name}-{option value}
		var (
			// parentCol *inouts.Column
			strs = strings.Split(curCol.CascadeName, "-")
		)

		// Is a parent and other cases
		if len(strs) <= 2 {
			afterCols = append(afterCols, curCol)
			continue
		}

		for _, aftCol := range afterCols {
			for _, option := range aftCol.Options {
				// Second Level
				if option.CascadeName == curCol.CascadeName {
					option.CascadeColumns = append(option.CascadeColumns, curCol)
					continue LOOP
				}

				// Third Level
				for _, cascadedCol := range option.CascadeColumns {
					for _, option := range cascadedCol.Options {
						if option.CascadeName == curCol.CascadeName {
							option.CascadeColumns = append(option.CascadeColumns, curCol)
							continue LOOP
						}
					}
				}
			}
		}
	}

	return
}

func HandleGroupRowColumns(cols []*inouts.Column) (groupColumns []*[]*inouts.Column) {

	groupColumns = []*[]*inouts.Column{}

LOOP:
	for _, curCol := range cols {

		if curCol.GroupName != "" {
			for _, columns := range groupColumns {
				for _, col := range *columns {
					if col.GroupName == curCol.GroupName {
						*columns = append(*columns, curCol)
						continue LOOP
					}
				}
			}
		}

		groupColumns = append(groupColumns, &[]*inouts.Column{curCol})
	}
	return
}

func filterLabel(label string) (result template.HTML) {

	label = strings.TrimSpace(label)

	if label == "undefined" {
		result = ""
	} else {
		result = template.HTML(label)
	}

	return
}

func DeleteSurveysByCaseIdHex(id string) (err error) {
	var (
		selector = bson.M{"caseid": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&SurveyNew{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

// utility function to compare new answer with the old one and update the privacy info properly!
//func CheckAndUpdatePrivacyFields(newAnswers, oldAnswers map[string][]string, match *Match) (err error) {
func CheckAndUpdatePrivacyFields(newAnswers map[string][]string, currentSurvey *SurveyNew, match *Match) (err error) {
	log.Println("[PRIVACY] >>> ===========================================")

	pubkeypath := configs.AppConf.GetPubKeyFile()
	log.Printf("[Debug] public key file path: %s\n", pubkeypath)

	var oldAnswers Answers = nil
	if currentSurvey != nil {
		oldAnswers = currentSurvey.Answers
		//log.Printf("[Debug] Old survey answers: %v\n\n", oldAnswers)
	}

	if newAnswers == nil {
		return errors.New("NULL newAnswers!")
	}
	if match == nil {
		return errors.New("NULL match!")
	}

	//log.Printf("[Debug] New survey answers: %v\n\n", newAnswers)

	// find all privacy fields
	for _, field := range match.Fields {
		if field == nil {
			continue
		}
		if field.IsPrivacy() {
			//log.Printf("[Debug] privacy field: %v\n", field.Name)
			//log.Printf("[Debug] new value: %v\n", newAnswers[field.Name])
			if oldAnswers != nil {
				//log.Printf("[Debug] old value: %v\n", oldAnswers[field.Name])
				if currentSurvey.IsStatusDraft() ||
					newAnswers[field.Name][0] != oldAnswers[field.Name][0] {
					if currentSurvey.IsStatusDraft() {
						log.Println("[New -> Draft Save to DB] Start encryption!")
					} else {
						log.Println("[Update DB] Start encryption!")
					}
					ciphertext, err := utils.Encrypt(newAnswers[field.Name][0], pubkeypath)
					if err != nil {
						log.Printf("[Debug] Error from encryption: %s\n", err)
						return err
					}
					newAnswers[field.Name][0] = ciphertext
				}
			} else {
				log.Println("[New -> Direct Save to DB] Start encryption!")
				ciphertext, err := utils.Encrypt(newAnswers[field.Name][0], pubkeypath)
				if err != nil {
					log.Printf("[Debug] Error from encryption: %s\n", err)
					return err
				}
				newAnswers[field.Name][0] = ciphertext
			}

		}
	}

	log.Println("<<< =========================================== [PRIVACY]")
	return
}
