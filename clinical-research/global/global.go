package global

import (
	"time"

	"regexp"
)

// Time (Mon Jan 2 15:04:05 2006)
const (
	TIME_LAYOUT_LONG    = `2006-01-02 15:04:05`
	TIME_LAYOUT_DEFAULT = `2006-01-02 15:04`
	TIME_LAYOUT_DATE    = `2006-01-02`

	TIME_LAYOUT_CN_DEFAULT   = `2006年01月02日 15:04`
	TIME_LAYOUT_DATE_COMPACT = `20060102`
)

const (
	DefaultPassword = "123456"
)

const (
	FormTextType      = "text"
	FormCheckboxType  = "checkbox-group"
	FormDateType      = "date"
	FormTextAreaType  = "textarea"
	FormSelectType    = "select"
	FormRadioType     = "radio-group"
	FormParagraphType = "paragraph"
	FormNumberType    = "number"
	FormHeaderType    = "header"

	GroupRowPrefix    = "group-row"
	CascadePrefix     = "cascade-"
	EmbedPrefix       = "embed-"
	OptionText        = "option-text"
	StatPrefix        = "stat-"
	ScorePrefix       = "score-"
	PrivacyPrefix     = "privacy-" // for privacy info to be encrypted
	SensitivePrefix   = "sensitive-"
	ValidateMinPrefix = "validate-min-"
	ValidateMaxPrefix = "validate-max-"
	Picker            = "picker-" // picker-date picker-time picker-datetime(default)
)

const (
	StatAge          = "stat-age"
	StatGender       = "stat-gender"
	StatIntervention = "stat-intervention"
	StatLeave        = "stat-leave"
	StatRanking      = "stat-ranking"
	StatWhere2Go     = "stat-where2go"
)

const (
	SensitiveUserName  = "sensitive-username"
	SensitiveAddress   = "sensitive-address"
	SensitivePhone     = "sensitive-phone"
	SensitiveEmail     = "sensitive-email"
	SensitiveID        = "sensitive-id"
	SensitiveCase      = "sensitive-case-no"
	SensitiveBed       = "sensitive-bed-no"
	SensitiveAdmission = "sensitive-admission-no"
)

// Case status
type CaseStatus string

const (
	CaseUnderway CaseStatus = "underway"
	CaseFinished CaseStatus = "finished"
)

var CaseStatusLabelMap = map[CaseStatus]string{
	CaseUnderway: "填写中",
	CaseFinished: "已提交",
}

// Match status
type MatchStatus string

const (
	MatchInit   MatchStatus = ""
	MatchDraft  MatchStatus = "draft"
	MatchCheck  MatchStatus = "check"
	MatchFinish MatchStatus = "finish"
)

var MatchStatusLabelMap = map[MatchStatus]string{
	MatchInit:   "未填",
	MatchDraft:  "暂存",
	MatchCheck:  "查错",
	MatchFinish: "完成",
}

var MatchStatusBadgeMap = map[MatchStatus]string{
	MatchInit:   "bg-danger",
	MatchCheck:  "bg-danger",
	MatchDraft:  "bg-warning",
	MatchFinish: "bg-success",
}

const (
	DEFAULT_PASSWORD         = "123456"
	DEFAULT_HOSPITAL_ZIPCODE = "110101"
)

const (
	ACCESS_CODE_EXPRIED_TIME = 5 * time.Minute
)

var (
	REGEXP_MOBILE = regexp.MustCompile(`^1[3|5|7|8|][0-9]{9}$`)
)

// Access state
const (
	ACCESS_REQUEST    = "request"
	ACCESS_VERIFIED   = "verified"
	ACCESS_IDENTIFIED = "identified"
	ACCESS_DELETED    = "deleted"
)

const (
	SMSSignature       = "Neuro通知"
	SMSCaseCreate      = "SMS_102240060"
	SMSMatch02         = "SMS_102375071"
	SMSMatch03Callback = "SMS_102315056"
)

// Scopes for getting statistics
const (
	ScopeStatAll  = "all"
	ScopeStatUser = "user"
	ScopeStatOrg  = "organizion"
)
