package models

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"github.com/kobeld/mgowrap"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func HasRegistered(mobile string) (yes bool, err error) {

	var (
		query = bson.M{
			"mobile":    mobile,
			"deletedat": bson.M{"$exists": false},
		}
	)

	return mgowrap.HasAny(&User{}, query)
}

func Login(mobile, password string) (user *User, err error) {

	user, err = GetUserByMobile(mobile)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	if user == nil {
		return
	}

	if !goutils.IsPwdMatch(user.Password, password) {
		return nil, nil
	}

	return
}

func Singup(user *User) (err error) {
	user.Password, err = goutils.EncryptPassword(user.Password)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}
	return mgowrap.Save(user, user.BeforeSave)
}

func GetUserByMobile(mobile string) (user *User, err error) {

	var (
		query = bson.M{
			"mobile":    mobile,
			"deletedat": bson.M{"$exists": false},
		}
	)

	err = mgowrap.Find(query, &user)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		debugutil.PrintStackAndError(err)
		return
	}
	return
}

func FindAllUsers(query bson.M) (users []*User, err error) {

	if query == nil {
		query = bson.M{}
	}

	query["deletedat"] = bson.M{"$exists": false}

	err = mgowrap.FindAll(query, &users)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func DeleteUserByIdHex(id string) (err error) {
	var (
		selector = bson.M{"_id": goutils.ToObjectIdOrBlank(id)}
		changer  = bson.M{"$set": bson.M{"deletedat": time.Now()}}
	)

	err = mgowrap.Update(&User{}, selector, changer)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func CreateUser(input *inouts.UserInput) (user *User, err error) {

	user = &User{
		Name:     input.Name,
		Mobile:   input.Mobile,
		Password: global.DefaultPassword,
		Role: Role{
			IsAdmin: input.IsAdmin,
		},
		IsSmsDisabled: input.IsSmsDisabled,
		OrgID:         goutils.ToObjectIdOrBlank(input.OrgID),
	}

	err = Singup(user)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}
