package webutil

import (
	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
)

func FlashParams(c echo.Context) (err error) {
	var (
		request  = c.Request()
		response = c.Response().Writer
	)

	sess, ok := c.Get(flashName).(*sessions.Session)
	if !ok {
		return sesNotFoundErr
	}

	formParams, err := c.FormParams()
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	sess.Values[paramsKey] = (map[string][]string)(formParams)

	err = sess.Save(request, response)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func FlashErrors(c echo.Context, errorMap map[string]string) (err error) {

	var (
		request  = c.Request()
		response = c.Response().Writer
	)

	sess, ok := c.Get(flashName).(*sessions.Session)
	if !ok {
		return
	}

	sess.Values[errorsKey] = errorMap

	err = sess.Save(request, response)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func FlashInfo(c echo.Context, key, value string) (err error) {

	var (
		request  = c.Request()
		response = c.Response().Writer
	)

	sess, ok := c.Get(flashName).(*sessions.Session)
	if !ok {
		return
	}

	info, ok := sess.Values[infoKey].(map[string]string)
	if !ok {
		info = make(map[string]string)
	}

	info[key] = value

	// Set the info back
	sess.Values[infoKey] = info

	err = sess.Save(request, response)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func GetFlashParams(c echo.Context) (params map[string][]string) {

	sess, ok := c.Get(flashName).(*sessions.Session)
	if !ok {
		return
	}

	if value, ok := sess.Values[paramsKey].(map[string][]string); ok {
		params = value
	}

	return
}
