package controllers

import (
	"errors"
	"net/http"

	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/webutil"
	"github.com/kobeld/mgowrap"

	"bitbucket.org/neumatic-sh/clinical-research/inouts"
	"bitbucket.org/neumatic-sh/clinical-research/models"
	"github.com/labstack/echo"
)

func GetCurrentOrg(c echo.Context) (org *models.Organization, err error) {

	var (
		user = GetUser(c)
	)

	if !user.IsValid() {
		err = errors.New("GetCurrentOrg error: current user is invalid!")
		debugutil.PrintStackAndError(err)
		return
	}

	err = mgowrap.FindById(user.OrgID, &org)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return
}

func ListOrgs(c echo.Context) (err error) {

	var (
		totalCases, totalSumitCases int

		allOrgs  = []*models.Organization{}
		allCases = []*models.Case{}
		allUsers = []*models.User{}
	)

	allOrgs, err = models.FindAllOrgs(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	allUsers, err = models.FindAllUsers(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	allCases, err = models.FindAllCases(nil)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	for _, org := range allOrgs {
		var (
			countCases, countSubmitCases int
			countUsers                   int
		)

		for _, user := range allUsers {
			if user.OrgID == org.Id {
				countUsers++
			}
		}

		for _, caze := range allCases {
			if caze.OrgID == org.Id {
				countCases++

				if caze.IsFinished() {
					countSubmitCases++
					totalSumitCases++
				}
			}
		}

		org.RenderData = map[string]interface{}{
			"countUsers":       countUsers,
			"countCases":       countCases,
			"countSubmitCases": countSubmitCases,
		}
	}

	totalCases = len(allCases)

	result := map[string]interface{}{
		"Orgs":               allOrgs,
		"TotalCases":         totalCases,
		"TotalSubmitCases":   totalSumitCases,
		"TotalUnderwayCases": totalCases - totalSumitCases,
	}

	return c.Render(http.StatusOK, "orgs/list", result)
}

func NewOrg(c echo.Context) (err error) {
	result := map[string]interface{}{
		"Org": &models.Organization{},
	}
	return c.Render(http.StatusOK, "orgs/new", result)
}

func CreateOrg(c echo.Context) (err error) {

	var (
		orgInput = &inouts.OrgInput{}
	)

	if err = c.Bind(orgInput); err != nil {
		return
	}

	orgInput.ValidateCreate()
	if !orgInput.HasErrorOn("Code") {
		hasOrg, err := models.HasOrgByCode(orgInput.Code)
		if debugutil.HasErrorAndPrintStack(err) {
			return err
		}

		if hasOrg {
			orgInput.AddError("Code", errorutil.ErrAlreadyExist)
		}
	}

	if orgInput.HasError() {
		webutil.FlashErrors(c, orgInput.ErrorMap)
		webutil.FlashParams(c)
		return c.Redirect(http.StatusFound, "/orgs/new")
	}

	_, err = models.CreateOrg(orgInput)
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	return c.Redirect(http.StatusFound, "/orgs")
}

func DeleteOrg(c echo.Context) (err error) {

	err = models.SoftDeleteOrgByIdHex(c.Param("id"))
	if debugutil.HasErrorAndPrintStack(err) {
		return
	}

	webutil.FlashInfo(c, "success", "删除成功！")
	return
}
