package models

import (
	"fmt"

	"bitbucket.org/neumatic-sh/clinical-research/global"
	"gopkg.in/mgo.v2/bson"
)

type Case struct {
	Base      `,inline`
	OrgID     bson.ObjectId
	CreatorID bson.ObjectId
	CaseTime  string    //张吉元新增创建病例时间
	Code      string
	CaseNumb  string    //张吉元新增病例编号
	Patient   Patient
	Status    global.CaseStatus
	IsInGroup bool
}

type Patient struct {
	FamilyName   string
	GivenName    string
	FamilyPinyin []string
	GivenPinyin  []string
}

func (this *Case) CollectionName() string {
	return "cases"
}

func (this *Case) StatusLabel() string {
	return global.CaseStatusLabelMap[this.Status]
}

func (this *Case) IsFinished() bool {
	return this.Status == global.CaseFinished
}

func (this *Case) IsUnderway() bool {
	return this.Status == global.CaseUnderway
}

func (this *Case) PatientName() string {
//	return fmt.Sprintf("%s%s", this.Patient.FamilyName, this.Patient.GivenName)
	return fmt.Sprintf("%s", this.Patient.FamilyName)
}

func (this *Case) PatientAcronym() (acronym string) {
	bytes := []byte{}

	for _, py := range this.Patient.FamilyPinyin {
		pyArr := []byte(py)

		if len(pyArr) == 0 {
			continue
		}
		bytes = append(bytes, pyArr[0])
	}

	for _, py := range this.Patient.GivenPinyin {
		pyArr := []byte(py)

		if len(pyArr) == 0 {
			continue
		}
		bytes = append(bytes, pyArr[0])
	}

	acronym = string(bytes)

	return
}
