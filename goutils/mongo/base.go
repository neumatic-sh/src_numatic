package mongo

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils"
	"bitbucket.org/neumatic-sh/goutils/global"
	"gopkg.in/mgo.v2/bson"
)

type Base struct {
	Id        bson.ObjectId `bson:"_id"`
	CreatedAt time.Time     `bson:",omitempty" json:"-"`
	UpdatedAt time.Time     `bson:",omitempty" json:"-"`
	DeletedAt time.Time     `bson:",omitempty" json:"-"`
}

func (this *Base) MakeId() interface{} {
	if this.Id == "" {
		this.Id = bson.NewObjectId()
	}
	return this.Id
}

func (this *Base) SetTime() {
	current := time.Now()
	if this.CreatedAt.IsZero() {
		this.CreatedAt = current
		this.UpdatedAt = current
	} else {
		this.UpdatedAt = current
	}
}

func (this *Base) IsFound() bool {
	return this.Id.Valid()
}

// Fields mapping for copying
func (this *Base) IdHex() string {
	return this.Id.Hex()
}

func (this *Base) CreatedAtStr() string {
	return goutils.FormatTime(this.CreatedAt, global.TIME_LAYOUT_DEFAULT)
}

func (this *Base) UpdatedAtStr() string {
	return goutils.FormatTime(this.UpdatedAt, global.TIME_LAYOUT_DEFAULT)
}

func (this *Base) CreatedAtDateStr() string {
	return goutils.FormatTime(this.CreatedAt, global.TIME_LAYOUT_DATE)
}

func (this *Base) UpdatedAtDateStr() string {
	return goutils.FormatTime(this.UpdatedAt, global.TIME_LAYOUT_DATE)
}
