#!/bin/sh
set -e

appPath="/root/app/clinical-research"
pidfile="/root/app/clinical-research/run.pid"

echo "Untar the views package"
cd $appPath
tar xzvf views.tar.gz

if [[ -f $pidfile ]]; then
	pid=$(cat $pidfile);
	if ps -p $pid > /dev/null; then
		echo "Kill old instance"
		kill -9 $pid; > /dev/null 2>&1;
	fi
fi

cp /root/app/main $appPath/

echo "Start new instance"
echo "start server -->" >> $appPath/app.log

nohup ./main -config=/root/app/clinical-research/configs/config.yaml -env=dev >> $appPath/app.log 2>&1 & echo $! > run.pid &

tail -f -n 10 $appPath/app.log


