package models

import "strings"
import "gopkg.in/mgo.v2/bson"

type User struct {
	Base          `,inline`
	Name          string
	FamilyName    string
	GivenName     string
	Email         string
	Mobile        string
	Password      string
	Avatar        string
	Address       string
	Desc          string
	State         string
	HasResetPwd   bool // Users created by the admin should reset their password first
	IsSmsDisabled bool
	Role          Role
	OrgID         bson.ObjectId          `bson:",omitempty"`
	RenderData    map[string]interface{} `bson:"-"`
}

func (this *User) CollectionName() string {
	return "users"
}

func (this *User) BeforeSave() {
	this.SetTime()
	this.Email = strings.ToLower(this.Email)
}

func (this *User) IsValid() bool {
	return true
}
