#!/bin/sh
set -e

pidfile="/home/app/research/clinical-research/run.pid"

if [[ -f $pidfile ]]; then
    pid=$(cat $pidfile);
    if ps -p $pid > /dev/null; then
        echo "Kill old instance with PID $pid"
        kill -9 $pid; > /dev/null 2>&1;
    fi
fi

echo "Done"