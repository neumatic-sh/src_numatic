package inouts

import (
	"time"

	"bitbucket.org/neumatic-sh/goutils/errorutil"
	"bitbucket.org/neumatic-sh/goutils/validutil"
	"bitbucket.org/neumatic-sh/clinical-research/global"
)

type Match02Input struct {
	validutil.ValidatorErr `,inline`
	HasDiagnosed           int    `json:"hasDiagnosed" form:"hasDiagnosed"` // 1: yes 2: no
	HasSigned              int    `json:"hasSigned" form:"hasSigned"`
	IsIncurable            int    `json:"isIncurable" form:"isIncurable"`
	IsLessFive             int    `json:"isLessFive" form:"isLessFive"`
	IsDecreased            int    `json:"isDecreased" form:"isDecreased"`
	IsIncorrigible         int    `json:"isIncorrigible" form:"isIncorrigible"`
	IsClear                int    `json:"isClear" form:"isClear"`
	InGroupAtStr           string `json:"inGroupAtStr" form:"inGroupAtStr"`
	InGroupAt              time.Time
	Status                 global.MatchStatus
}

func (this *Match02Input) ValidateSubmit() bool {

	optionMap := BuildOptionMap(1, 2)

	if _, ok := optionMap[this.HasDiagnosed]; !ok {
		this.AddError("hasDiagnosed", errorutil.ErrNoSelection)
	}

	if _, ok := optionMap[this.HasSigned]; !ok {
		this.AddError("hasSigned", errorutil.ErrNoSelection)
	}

	if _, ok := optionMap[this.IsIncurable]; !ok {
		this.AddError("isIncurable", errorutil.ErrNoSelection)
	}

	if _, ok := optionMap[this.IsLessFive]; !ok {
		this.AddError("isLessFive", errorutil.ErrNoSelection)
	}

	if _, ok := optionMap[this.IsDecreased]; !ok {
		this.AddError("isDecreased", errorutil.ErrNoSelection)
	}

	if _, ok := optionMap[this.IsClear]; !ok {
		this.AddError("isClear", errorutil.ErrNoSelection)
	}

	if _, ok := optionMap[this.IsIncorrigible]; !ok {
		this.AddError("isIncorrigible", errorutil.ErrNoSelection)
	}

	this.ValidField(this.InGroupAtStr, "inGroupAtStr", "nonzero", errorutil.ErrShouldNotBeBlank.Error())
	if this.InGroupAtStr != "" {
		var err error
		this.InGroupAt, err = time.Parse(global.TIME_LAYOUT_DATE, this.InGroupAtStr)
		if err != nil {
			this.AddError("inGroupAtStr", errorutil.ErrInvalidTimeFormat)
		}
	}

	return this.HasError()
}
