package utils

import (
	"errors"
	"log"
	"strings"

	"bitbucket.org/neumatic-sh/goutils/debugutil"
	"bitbucket.org/neumatic-sh/clinical-research/configs"
	"bitbucket.org/neumatic-sh/clinical-research/global"
	"github.com/316014408/alidayu"
)

func SendSMS(tmpl, mobile, paramStr string, signature ...string) (err error) {

	if mobile == "" {
		log.Println("[SMS] Mobile number is blank!")
		return
	}

	if tmpl == "" {
		log.Println("[SMS] Template is blank!")
		return
	}

	log.Printf("[SMS] Sent: %s %s %s\n", mobile, tmpl, paramStr)

	// Ignore those demo mobiles
	// if strings.Contains(mobile, "1888") {
	// 	return
	// }

	if configs.AppConf.SmsDisabled {
		log.Println("[SMS] The sending is disabled.")
		return
	}

	smsSignature := global.SMSSignature
	if len(signature) > 0 {
		smsSignature = signature[0]
	}

	success, resp := alidayu.SendSMS(mobile, smsSignature, tmpl, paramStr)
	if !success {
		err = errors.New(resp)
		debugutil.PrintStackButSwallowError(&err)
	}

	for _, m := range strings.Split(configs.AppConf.SmsMobile, " ") {
		if len(m) != 11 && m == mobile {
			continue
		}

		log.Printf("[SMS] Batch sent: %s\n", mobile)

		success, resp = alidayu.SendSMS(m, smsSignature, tmpl, paramStr)
		if !success {
			err = errors.New(resp)
			debugutil.PrintStackButSwallowError(&err)
		}
	}

	return
}
